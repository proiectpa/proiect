#include "pch.h"
#include "../Ball_Framework/Ball.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BallTests
{
	TEST_CLASS(BallTests)
	{
	public:
		
		TEST_METHOD(BallConstructor)
		{
			sf::Texture texture;
			texture.loadFromFile("../Resources/Ball.png");
			Ball ball(10, 10, 10, sf::Color::Red, texture);
			Assert::IsTrue(ball.GetX() == 10);
			Assert::IsTrue(ball.GetY() == 10);
			Assert::IsTrue(ball.GetRadius() == 10);
			Assert::IsTrue(ball.GetColor() == sf::Color::Red);
			Assert::IsNotNull(&texture);
		}
		TEST_METHOD(BallDirection)
		{
			sf::Vector2f ballDirection(300, 300);
			sf::Texture texture;
			texture.loadFromFile("../Resources/Ball.png");
			Ball ball(10, 10, 10, sf::Color::Red, texture);
			ball.SetBallDirection(ballDirection);
			Assert::AreEqual(ball.GetBallDirection().x, ballDirection.x);
			Assert::AreEqual(ball.GetBallDirection().y, ballDirection.y);
		}
		TEST_METHOD(BallGlobalBounds)
		{
			sf::Texture texture;
			texture.loadFromFile("../Resources/Ball.png");
			Ball ball(10, 10, 10, sf::Color::Red, texture);
			sf::FloatRect ballGlobalBounds = ball.GetGlobalBounds();
			Assert::IsNotNull(&ballGlobalBounds);
		}
	};
}
