#include "pch.h"
#include "../Ball_Framework/Paddle.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PaddleTests
{
	TEST_CLASS(PaddleTests)
	{
	public:
		TEST_METHOD(PaddleConstructor)
		{
			sf::Vector2f position(10, 750);
			sf::Vector2f dimension(50, 30);
			sf::Texture texture;
			texture.loadFromFile("../Resources/Paddle.png");
			Paddle paddle(position,dimension,texture);
			Assert::IsTrue(paddle.GetX() == position.x);
			Assert::IsTrue(paddle.GetY() == position.y);
			Assert::IsTrue(paddle.GetLenght() == dimension.x);
			Assert::IsTrue(paddle.GetHeight() == dimension.y);
			if (texture.loadFromFile("../Resources/Paddle.png"))
			{
				Assert::Fail(L"Texture not loaded from file.");
			}
			
		}
		TEST_METHOD(PaddleGloblaBounds)
		{
			sf::Texture texture;
			texture.loadFromFile("../Resources/Paddle.png");
			Paddle paddle(sf::Vector2f(10, 750), sf::Vector2f(50, 30), texture);
			sf::FloatRect paddleGlobalBounds = paddle.GetGlobalBounds();
			Assert::IsNotNull(&paddleGlobalBounds);
		}
	};
}