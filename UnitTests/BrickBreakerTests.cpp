#include "pch.h"
#include "../Ball_Framework/BrickBreaker.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BrickBreakerTests
{
	TEST_CLASS(BrickBreakerTests)
	{
	public:
		TEST_METHOD(BrickBreakerConstructor)
		{
			uint8_t difficulty = 2;
			uint8_t nrBricks = (5 + difficulty) * (2+ difficulty);
			Game::MultiplayerType multiplayerType=Game::MultiplayerType::Collaborator;
			std::string player1 = "Player1";
			std::string player2 = "Player2";
			std::unique_ptr <Game> brickBreaker=std::make_unique<BrickBreaker>(difficulty, 2, multiplayerType, player1, player2);
			Assert::IsNotNull(&brickBreaker->GetMultiplayerType());
			Assert::IsTrue(brickBreaker->GetPlayers().size() == 2);
			Assert::IsTrue(brickBreaker->GetBricks().size() == nrBricks);
			Assert::AreEqual(brickBreaker->GetPlayers()[0].GetName(), player1);
			Assert::AreEqual(brickBreaker->GetPlayers()[1].GetName(), player2);
			Assert::IsTrue(brickBreaker->GetBalls().size()>0);
		}
	};
}