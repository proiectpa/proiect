#include "pch.h"
#include "../Ball_Framework/Pong.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PongTests
{
	TEST_CLASS(PongTests)
	{
	public:
		TEST_METHOD(PongConstructor)
		{
			uint8_t difficulty = 2;
			int nrBricks = 5  + (3 * difficulty);
			Game::MultiplayerType multiplayerType = Game::MultiplayerType::HumanOponent;
			std::string player1 = "Player1";
			std::string player2 = "Player2";
			std::unique_ptr <Game> pong = std::make_unique<Pong>(difficulty, 2, multiplayerType, player1, player2);
			Assert::IsNotNull(&pong->GetMultiplayerType());
			Assert::IsTrue(pong->GetPlayers().size() == 2);
			Assert::IsTrue(pong->GetBricks().size() == nrBricks);
			Assert::AreEqual(pong->GetPlayers()[0].GetName(), player1);
			Assert::AreEqual(pong->GetPlayers()[1].GetName(), player2);
			Assert::IsTrue(pong->GetBalls().size() > 0);
		}
	};
}