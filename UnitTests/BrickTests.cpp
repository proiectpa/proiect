#include "pch.h"
#include "../Ball_Framework/Brick.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BrickTests
{
	TEST_CLASS(BrickTests)
	{
	public:

		TEST_METHOD(BrickConstructor)
		{
			sf::Texture texture;
			//texture.loadFromFile("../Resources/Brick.jpg");
			sf::Vector2f position(450, 300);
			sf::Vector2f dimension(50, 50);
			sf::Color color(255, 100, 100, 255);
			Brick brick(position, dimension, 1, color, Brick::Effect::BrickBecomesBall, texture);
			Assert::IsTrue(brick.GetCoordX() == position.x);
			Assert::IsTrue(brick.GetCoordY() == position.y);
			Assert::IsTrue(brick.GetGlobalBounds().width == dimension.x);
			Assert::IsTrue(brick.GetGlobalBounds().height == dimension.y);
			Assert::IsTrue(brick.GetColor() == color);
			if (texture.loadFromFile("../Resources/Brick.jpg"))
			{
				Assert::Fail(L"Texture not loaded from file.");
			}
		}
		TEST_METHOD(DecreaseDurability)
		{
			uint8_t durability = 1;
			sf::Texture texture;
			texture.loadFromFile("../Resources/Brick.jpg");
			Brick brick(sf::Vector2f(450, 300), sf::Vector2f(50, 50), durability, sf::Color(255, 100, 100, 255), Brick::Effect::BrickBecomesBall, texture);
			brick.DecreaseDurability();
			Assert::AreEqual(brick.GetDurability(), --durability);
		}
		TEST_METHOD(GenerateId)
		{
			std::string id="";
			sf::Texture texture;
			texture.loadFromFile("../Resources/Brick.jpg");
			Brick brick(sf::Vector2f(450, 300), sf::Vector2f(50, 50),1, sf::Color(255, 100, 100, 255), Brick::Effect::BrickBecomesBall, texture);
			id+= std::to_string(brick.GetCoordX()) + 'x';
			id+= std::to_string(brick.GetCoordY()) + 'y';
			Assert::AreEqual(brick.GetID(), id);

		}
	};
}