#include "pch.h"
#include "../Ball_Framework/Player.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PlayerTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(PlayerConstructor)
		{
			sf::Vector2f position(400,600);
			sf::Vector2f dimension(50,30);
			sf::Font score_font;
			score_font.loadFromFile("../Resources/arial.ttf");
			std::string name = "Player";
			sf::Texture texture;
			score_font.loadFromFile("../Resources/Paddle.png");
			sf::Texture heart;
			score_font.loadFromFile("../Resources/Hearts.png");
			Player player(position, dimension, score_font, name, texture, heart);
			Assert::IsNotNull(&player.GetPaddle());
			Assert::AreEqual(player.GetPaddle().GetY(),position.y);
			Assert::AreEqual(player.GetPaddle().GetHeight(), dimension.y);
			Assert::AreEqual(player.GetPaddle().GetLenght(), dimension.x);
			Assert::AreEqual(player.GetName(), name);
			Assert::IsNotNull(&texture);
			if (texture.loadFromFile("../Resources/Paddle.png"))
			{
				Assert::Fail(L"Texture not loaded from file.");
			}
			
			if (heart.loadFromFile("../Resources/Hearts.png"))
			{
				Assert::Fail(L"Texture not loaded from file.");
			}
		}
		TEST_METHOD(PlayerSpeed)
		{
			sf::Vector2f position(400, 600);
			sf::Vector2f dimension(50, 30);
			sf::Font score_font;
			score_font.loadFromFile("../Resources/arial.ttf");
			std::string name = "Player";
			sf::Texture texture;
			score_font.loadFromFile("../Resources/Paddle.png");
			sf::Texture heart;
			score_font.loadFromFile("../Resources/Hearts.png");
			Player player(position, dimension, score_font, name, texture, heart);
			player.SetSpeed(5);
			Assert::IsTrue(player.GetSpeed() == 5);

		}

		TEST_METHOD(PlayerBricksDestroyed)
		{
			sf::Vector2f position(400, 600);
			sf::Vector2f dimension(50, 30);
			sf::Font score_font;
			score_font.loadFromFile("../Resources/arial.ttf");
			std::string name = "Player";
			sf::Texture texture;
			score_font.loadFromFile("../Resources/Paddle.png");
			sf::Texture heart;
			score_font.loadFromFile("../Resources/Hearts.png");
			Player player(position, dimension, score_font, name, texture, heart);
			uint8_t destroyedBricks=player.GetDestroyedBricks();
			player.AddDestroyedBrick();
			Assert::IsTrue(++destroyedBricks == player.GetDestroyedBricks());

		}

		TEST_METHOD(PlayerScore)
		{
			sf::Vector2f position(400, 600);
			sf::Vector2f dimension(50, 30);
			sf::Font score_font;
			score_font.loadFromFile("../Resources/arial.ttf");
			std::string name = "Player";
			sf::Texture texture;
			score_font.loadFromFile("../Resources/Paddle.png");
			sf::Texture heart;
			score_font.loadFromFile("../Resources/Hearts.png");
			Player player(position, dimension, score_font, name, texture, heart);
			uint16_t score = player.GetScore();
			player.AddPointsToScore(1);
			Assert::IsTrue(++score == player.GetScore());

		}
	};
}