#include "pch.h"
#include "../Ball_Framework/OurWindow.h"
#include "../ThirdParty/include/SFML/Graphics.hpp"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace OurWindowTests
{
	TEST_CLASS(OurWindowTests)
	{
	public:
		TEST_METHOD(OurWindowConstructor)
		{
			sf::RenderWindow window(sf::VideoMode(WindowWidth, WindowHeight), "Ball Framework", sf::Style::Close);
			OurWindow ourWindow;
			Assert::AreEqual(window.getSize().x, ourWindow.GetWindow().getSize().x);
			Assert::AreEqual(window.getSize().y, ourWindow.GetWindow().getSize().y);
		}
	};
}