#include "Logging.h"
#include <ctime>
#include <chrono>

void Logger::log(const char* message, Level level)
{
	static std::ofstream os("log.txt",std::ofstream::app);
	static Level minimumLevel = Level::Info;

	if (static_cast<int>(level) < static_cast<int>(minimumLevel))
		return;

	time_t now = time(NULL);
	tm now_tm = {};
	char str[26] = {};
	localtime_s(&now_tm, &now);
	asctime_s(str, 26, &now_tm);

	os <<"Time: " << str << "   [" << LogLevelToString(level) << "] " << message << std::endl;
};

void Logger::log(const std::string& message, Level level)
{
	log(message.c_str(), level);
}

const char* Logger::LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}