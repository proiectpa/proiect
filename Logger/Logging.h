#pragma once

#include <iostream>
#include <fstream>
#include <string>

#ifdef LOGGING_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
#define LOGGING_API __declspec(dllimport)
#endif //LOGGING_EXPORTS


class LOGGING_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

	static void log(const char* message, Level level);

	static void log(const std::string& message, Level level);

	static const char* LogLevelToString(Logger::Level level);
};

