#include "Player.h"
#include <iostream>
#include "Game.h"

Player::Player(sf::Vector2f position, sf::Vector2f dimension, sf::Font& score_font, std::string name, sf::Texture& texture, sf::Texture& heart)
{
	m_paddle = Paddle(position, dimension, texture);
	m_speed = 0;
	m_lives = 5;
	m_score = 0;
	m_name = name;
	m_isWinner = true;
	m_destroyedBricks = 0;
	m_textScore.setFont(score_font);
	m_textScore.setCharacterSize(CharacterSize);
	m_textScore.setFillColor(sf::Color::Transparent);
	m_textScore.setOutlineThickness(OutLineThickness);
	m_textScore.setOutlineColor(sf::Color::White);
	try
	{
		m_textScore.setString(m_name + ":\n" + std::to_string(m_score));
		throw std::exception("Message!");
	}
	catch (std::exception& exception)
	{}
	m_hearts.setTexture(heart);
}

void Player::Move(sf::Vector2f vector)
{
	this->m_paddle.MovePaddle(vector);
}

Paddle& Player::GetPaddle()
{
	return this->m_paddle;
}

const float& Player::GetSpeed() const
{
	return m_speed;
}

const bool& Player::IsWinner() const
{
	return m_isWinner;
}

const uint8_t Player::GetDestroyedBricks() const
{
	return m_destroyedBricks;
}

const uint8_t& Player::GetLives() const
{
	return m_lives;
}

sf::Text* Player::GetText()
{
	return &m_textScore;
}

const uint16_t& Player::GetScore() const
{
	return m_score;
}

const std::string& Player::GetName() const
{
	return m_name;
}

sf::Sprite& Player::GetHearts()
{
	return m_hearts;
}

void Player::SetSpeed(float speed)
{
	m_speed = speed;
}

void Player::SetAsLoser()
{
	m_isWinner = false;
}

void Player::AddDestroyedBrick()
{
	++m_destroyedBricks;
}

void Player::SetPaddleDimension(sf::Vector2f dimension)
{
	m_paddle.SetDimension(dimension);
}

void Player::IncreaseSpeed()
{
	m_speed *= 1.2;
}

void Player::DecreaseSpeed()
{
	m_speed *= 0.8;
}

void Player::DecreaseLives()
{
	--m_lives;
	m_hearts.setTextureRect(sf::IntRect(0, 0, 11 + 16 * m_lives, 25));
}

void Player::AddPointsToScore(uint16_t points)
{
	m_score += points;
	try
	{
      	m_textScore.setString(m_name + ":\n" + std::to_string(m_score));
		throw std::exception("Message!");
	}
	catch (std::exception& exception)
	{}
}
