#pragma once
#include "OurWindow.h"
#include "Game.h"
#include "Button.h"
#include "BrickBreaker.h"
#include "Pong.h"
#include <array>
#include "Statistic.h"

class GameRender
{
public:
	GameRender(std::shared_ptr <OurWindow> window);
	void Render(Game& game);
	void GameOver(Game& game);
	void PauseMenu(Game& game);

	OurWindow& GetWindow();
private:
	std::shared_ptr<OurWindow> m_window;
	sf::Texture m_button_texture;
	sf::Texture m_background_texture;
	sf::Font m_font;
	sf::Sprite m_background;
	sf::Text m_score;
	sf::Text m_winner_text;
	Statistic m_statistic;
};

