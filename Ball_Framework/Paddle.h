#pragma once
#include "SFML\Graphics.hpp"

class Paddle: public sf::Drawable
{
public:
	Paddle()=default;
	Paddle(sf::Vector2f position, sf::Vector2f dimension, sf::Texture& texture);
	~Paddle();

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void MovePaddle(sf::Vector2f vector);

	void SetPosition(sf::Vector2f position);
	void SetDimension(sf::Vector2f dimension);  
	void SetColor(const sf::Color& color);

	const float& GetX() const;
	const float& GetY() const;
	const float& GetHeight() const;
	const float& GetLenght() const;
	sf::FloatRect GetGlobalBounds();
	const sf::Color& GetColor() const;
private:
	sf::Color m_color;
	sf::RectangleShape m_paddle;
};

