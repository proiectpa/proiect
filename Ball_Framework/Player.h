#pragma once
#include <cstdint>
#include "Paddle.h"

const int CharacterSize = 40;
const int OutLineThickness = 1;

class Player
{
public:
	Player() = default;
	Player(sf::Vector2f position, sf::Vector2f dimension, sf::Font& score_font, std::string name, sf::Texture& texture, sf::Texture& heart);
	void Move(sf::Vector2f vector);

	Paddle& GetPaddle();
	const float& GetSpeed() const;
	const bool& IsWinner() const;
	const uint8_t GetDestroyedBricks() const;
	const uint8_t& GetLives()const;
	sf::Text* GetText();
	const uint16_t& GetScore() const;
	const std::string& GetName() const;
	sf::Sprite& GetHearts();

	void SetSpeed(float speed);
	void SetAsLoser();
	void AddDestroyedBrick();
	void SetPaddleDimension(sf::Vector2f dimension);

	void IncreaseSpeed();
	void DecreaseSpeed();
	void DecreaseLives();
	void AddPointsToScore(uint16_t points);

private:
	std::string m_name;
	sf::Text m_textScore;
	sf::Sprite m_hearts;
	uint16_t m_score;
	uint8_t m_destroyedBricks;
	Paddle m_paddle;
	float m_speed;
	uint8_t m_lives;
	bool m_isWinner;
};

