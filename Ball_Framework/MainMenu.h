#pragma once
#include <regex>
#include "OurWindow.h"
#include "Game.h"
#include "Button.h"
#include "BrickBreaker.h"
#include "Pong.h"
#include "GameRender.h"
#include "Textbox.h"

const std::regex m_expresion("[A-Za-z0-9]*");

class MainMenu
{
public:
	MainMenu(std::shared_ptr<OurWindow> window);

	void ChooseGame();
	void ChooseDifficulty();
	void ChooseNrPlayers();
	void ChooseMultiplayerType();
	void ChooseNames(bool& moveOn, bool& changedNrPlayers);
	void Instructions();
	void StartGame();

	void RunMenu();

private:
	bool m_changedNrPlayers = true;
	uint8_t m_nrPlayers;
	uint8_t m_difficulty;
	std::shared_ptr <Game> m_game;
	std::shared_ptr<OurWindow> m_window;
	Button m_previous;
	std::string m_game_type;
	Game::MultiplayerType m_multiplayer_type;
	std::string m_player1;
	std::string m_player2;
	sf::Font m_font;
	sf::Texture m_button_texture;
	sf::Texture m_background_texture;
	sf::Sprite m_background;
	Button m_instructions;
	sf::Sprite m_background_instructions;
	sf::Texture m_instructions_texture;
};

