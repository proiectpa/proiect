#pragma once
#include "Game.h"
#include"OurWindow.h"

const float InitialPaddleY_BB = WindowHeight-PaddleHeight-5;
const float InitialLeftPaddleX_BB = WindowWidth/3;
const float InitialRightPaddleX_BB = WindowWidth - InitialLeftPaddleX_BB - PaddleLenght;

class BrickBreaker : public Game
{
public:
	BrickBreaker(uint8_t difficulty, uint8_t nrPlayers, MultiplayerType multiplayer_type, std::string player1, std::string player2);
	
	void Restart() override;
	void BallWindowCollision(Ball& ball)override;
	void ResetEffect(Ball& ball, Player& player, Brick::Effect effect)override;
	void ApplyEffect(Brick::Effect effect, Ball& ball, Player& player)override;
	void Update(float delta) override;
	void InitializeGame() override;
	void WindowColision() override;
	void PlayerInput(float delta) override;
	void CalculateScore(const Brick& brick, Player& player) override;
	void BallPaddleCollision(Ball& ball, Paddle& paddle, float testX, float testY);
	void BallBrickCollsision(Ball& ball, Brick& brick, float testX, float testY);
	void CheckGameWon() override;
	void SetGameOver(sf::Text& score, sf::Text& winner)override;
	sf::Vector2f ResetBallDirection(const Ball& ball) override;

private:
	uint8_t m_targetNrBricks;
};
