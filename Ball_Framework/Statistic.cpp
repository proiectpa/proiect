#include "Statistic.h"
#include<sstream>
#include <iostream>
#include <iomanip>
#include "../Logger/Logging.h"

Statistic::Statistic() :
	m_Pong("Pong.txt"),
	m_BrickBreaker("BrickBreaker.txt")
{
}

void Statistic::UpdateStatisitics(Game& game)
{
	std::ifstream input;
	std::ofstream output;
	if (game.GetGameType() == Game::GameType::BrickBreaker)
	{
		input.open(m_BrickBreaker);
		ReadStatistic(input);
	}
	else if (game.GetGameType() == Game::GameType::Pong)
	{
		input.open(m_Pong);
		ReadStatistic(input);
	}

	std::vector<Player> players = game.GetPlayers();
	bool found;
	for (const auto& player : players)
	{
		found = false;
		if (player.GetName() != "Player1" && player.GetName() != "Player2")
		{
			for (auto& stat : m_playerStats)
			{
				if (stat.m_name == player.GetName())
				{
					uint8_t score= player.GetScore();
					stat.m_maximScore = std::max(stat.m_maximScore, score);
					found = true;
					++stat.m_nrPlayedGames;
					if (player.IsWinner())
					{
						++stat.m_nrWonGames;
					}
				}
			}
			if (found==false)
			{
				PlayerStats temporary(player.GetName(), 1, 0, player.GetScore());
				if (player.IsWinner())
				{
					temporary.m_nrWonGames = 1;
				}
				m_playerStats.push_back(temporary);
			}
		}

	}

	input.close();
	if (game.GetGameType() == Game::GameType::BrickBreaker)
	{
		output.open(m_BrickBreaker);
		WriteStatistic(output);
	}
	else if(game.GetGameType() == Game::GameType::Pong)
	{
		output.open(m_Pong);
		WriteStatistic(output);
	}
	Logger::log(std::string("Statistics for ") + GameTypeToString(game.GetGameType()) + std::string(" updated"), Logger::Level::Info);
}

const char* Statistic::GameTypeToString(Game::GameType gameType)
{
	switch (gameType)
	{
	case Game::GameType::BrickBreaker:
		return "BrickBreaker";
	case Game::GameType::Pong:
		return "Pong";
	default:
		return "";
	}
}

void Statistic::WriteStatistic(std::ofstream& gameType)
{
	gameType << "Name              Played    Won       TopScore\n";
	for (const auto& stat : m_playerStats)
	{
		gameType << std::setiosflags(std::ios::left)
				 << std::setw(18) << stat.m_name
				 << std::setw(10) << stat.m_nrPlayedGames
				 << std::setw(10) << stat.m_nrWonGames
				 << std::setw(10) << (int)stat.m_maximScore << '\n';
	}
	m_playerStats.clear();
}

void Statistic::ReadStatistic(std::ifstream& gameType)
{
	std::string name;
	int nrPlayedGames;
	int nrWonGames;
	int maximScore;
	std::string line;
	std::stringstream stream;
	std::getline(gameType, line);
	while (std::getline(gameType, line))
	{
		stream.str(line);
		stream >> name >> nrPlayedGames >> nrWonGames >> maximScore;
		m_playerStats.push_back(PlayerStats(name, nrPlayedGames, nrWonGames, maximScore));
		stream.str("");
		stream.clear();
	}
	gameType.clear();
}
