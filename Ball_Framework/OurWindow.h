#pragma once
#include <SFML/Graphics.hpp>

const float WindowHeight = 720;
const float WindowWidth = 1280;
const float FrameLimit = 60;

class OurWindow
{
	
public:
	OurWindow();
    void ClosingWindow(sf::Event event);
	sf::RenderWindow &GetWindow();
private:
	sf::RenderWindow m_window;
};

