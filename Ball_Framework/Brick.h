#pragma once
#include "SFML\Graphics.hpp"

class Brick :public sf::Drawable
{
public:

	enum class Effect
	{
		PaddleIncreaseSize = 1,
		PaddleIncreaseSpeed = 2,
		BallDecreaseSpeed = 3,

		NoEffect = 0,

		BallIncreaseSpeed = -1,
		PaddleDecreaseSize = -2,
		PaddleDecreaseSpeed = -3,
		BrickBecomesBall = -4
	};
	const char* EffectToString();
	Brick() = default;
	Brick(sf::Vector2f position, sf::Vector2f dimension, uint8_t durability, sf::Color color, Effect effect, sf::Texture& texture);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void SetPosition(float coordX, float coordY);
	void UpdateEffectPosition();
	void SetColor(sf::Color color);
	void SetTexture(sf::Texture& texture);
	void SetEffectTexture(sf::Texture& texture);
	void SetEffect(Brick::Effect effect);
	void DecreaseDurability();
	void SetSizeBrick(sf::Vector2f vector);

	const float& GetCoordX() const;
	const float& GetCoordY() const;
	const Effect& GetEffect() const;
	const sf::FloatRect GetGlobalBounds()const;
	void GenerateID();
	const std::string& GetID() const;
	const uint8_t& GetDurability() const;
	const sf::Color& GetColor() const;
private:
	sf::RectangleShape m_brick;
	sf::Color m_color;
	sf::Sprite m_effectSprite;
	uint8_t m_durability;
	Effect m_effect;
	std::string m_ID;
};

