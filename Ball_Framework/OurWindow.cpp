#include "OurWindow.h"

OurWindow::OurWindow() :
	m_window(sf::VideoMode(WindowWidth, WindowHeight), "Ball Framework", sf::Style::Close)
{
}

void OurWindow::ClosingWindow(sf::Event event)
{
	switch (event.type)
	{
	case sf::Event::Closed:
		m_window.close();
		break;
	case sf::Event::KeyPressed:
		if (event.key.code == sf::Keyboard::Escape)
		{
			m_window.close();
		}
		break;
	}
}

sf::RenderWindow& OurWindow::GetWindow()
{
	return m_window;
}
