#include "Pong.h"
#include "../Logger/Logging.h"
#include <thread>
#include <iostream>

static sf::Clock timer;

Pong::Pong(uint8_t difficulty, uint8_t nrPlayers, MultiplayerType multiplayer_type, std::string player1, std::string player2) :Game(difficulty)
{
	m_balls.push_back(Ball(BallCoordX, BallCoordY, RadiusBall, CustomRed, m_textures["Ball"]));
	m_gameType = Game::GameType::Pong;
	m_nrPlayers = nrPlayers;
	m_multiplayerType = multiplayer_type;


	m_players.push_back(Player(sf::Vector2f(InitialRightPaddleX_Pong, InitialPaddleY_Pong), sf::Vector2f(PaddleHeight, PaddleLenght), m_scoreFont, player1, m_textures["PaddleR"], m_textures["Hearts"]));
	m_players[0].SetSpeed(InitialPaddleSpeed);
	m_players[0].GetText()->setPosition(sf::Vector2f(WindowWidth - WindowWidth / 3, WindowHeight - (WindowHeight / 1.7)));
	m_players[0].GetHearts().setPosition(sf::Vector2f(WindowWidth - WindowWidth / 3, WindowHeight - (WindowHeight / 1.7) + m_players[0].GetText()->getGlobalBounds().height + 20));
	m_players[0].GetPaddle().SetColor(CustomRed);

	m_players.push_back(Player(sf::Vector2f(InitialLeftPaddleX_Pong, InitialPaddleY_Pong), sf::Vector2f(PaddleHeight, PaddleLenght), m_scoreFont, player2, m_textures["PaddleL"], m_textures["Hearts"]));
	m_players[1].SetSpeed(InitialPaddleSpeed);
	m_players[1].GetText()->setPosition(sf::Vector2f(WindowWidth / 6, WindowHeight - (WindowHeight / 1.7)));
	m_players[1].GetHearts().setPosition(sf::Vector2f(WindowWidth / 6, WindowHeight - (WindowHeight / 1.7) + m_players[0].GetText()->getGlobalBounds().height + 20));
	m_players[1].GetPaddle().SetColor(CustomBlue);

	InitializeGame();
}

void Pong::Restart()
{
	m_gameState = GameState::Playing;
	m_bricks.clear();
	m_effects.clear();

	std::vector<Player> players;
	float coord = InitialRightPaddleX_Pong;
	sf::Vector2f text_pos(WindowWidth - WindowWidth / 3, WindowHeight - (WindowHeight / 1.7));
	std::string texture = "PaddleR";

	m_balls.clear();
	m_balls.push_back(Ball(BallCoordX, BallCoordY, RadiusBall, CustomRed, m_textures["Ball"]));
	m_balls[0].SetColor(m_players[0].GetPaddle().GetColor());

	int index = 0;
	for (auto& player : m_players)
	{
		players.push_back(Player(sf::Vector2f(coord, InitialPaddleY_Pong), sf::Vector2f(PaddleHeight, PaddleLenght), m_scoreFont, player.GetName(), m_textures[texture], m_textures["Hearts"]));
		coord = InitialLeftPaddleX_Pong;
		players[index].GetText()->setPosition(text_pos);
		players[index].GetPaddle().SetColor(m_players[index].GetPaddle().GetColor());
		players[index].GetHearts().setPosition(text_pos.x, text_pos.y + m_players[index].GetText()->getGlobalBounds().height + 20);
		text_pos.x = WindowWidth / 6;
		texture = "PaddleL";
		players[index++].SetSpeed(InitialPaddleSpeed);
	}
	m_players.clear();
	m_players = players;

	InitializeGame();
}

void Pong::BallWindowCollision(Ball& ball)
{
	if (m_balls.size() > 1)
	{
		for (int i = 0; i < m_balls.size(); i++)
		{
			if (&m_balls[i] == &ball)
			{
				m_balls.erase(m_balls.begin() + i);
				break;
			}
		}
	}
	else
	{
		float newX = InitialBallSpeed * 2;
		if (ball.GetBallDirection().x < 0)
		{
			newX = -newX;
		}
		if (ball.GetX() < BallCoordX)
		{
			m_players[1].DecreaseLives();
			m_players[0].AddPointsToScore(1);
		}
		else
		{
			m_players[0].DecreaseLives();
			m_players[1].AddPointsToScore(1);
		}
		ball.SetPosition(BallCoordX, BallCoordY);
		ball.SetBallDirection(sf::Vector2f(newX, 0));
	}
}

void Pong::ResetEffect(Ball& ball, Player& player, Brick::Effect effect)
{
	sf::sleep(sf::seconds(5));
	switch (effect)
	{
	case Brick::Effect::PaddleIncreaseSize:
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(PaddleHeight, PaddleLenght));
		}
		break;
	case Brick::Effect::PaddleDecreaseSize:
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(PaddleHeight, PaddleLenght));
		}
		break;
	case Brick::Effect::PaddleIncreaseSpeed:
		if (&player != nullptr)
		{
			player.SetSpeed(InitialPaddleSpeed);
		}
		break;
	case Brick::Effect::BallDecreaseSpeed:
		if (&ball != nullptr)
		{
			ball.SetBallDirection(ResetBallDirection(ball));
		}
		break;
	case Brick::Effect::BallIncreaseSpeed:
		if (&ball != nullptr)
		{
			ball.SetBallDirection(ResetBallDirection(ball));
		}
		break;
	case Brick::Effect::PaddleDecreaseSpeed:
		if (&player != nullptr)
		{
			player.SetSpeed(InitialPaddleSpeed);
		}
		break;
	default:
		break;
	}
	m_effects.erase(effect);
}

void Pong::ApplyEffect(Brick::Effect effect, Ball& ball, Player& player)
{
	switch (effect)
	{
	case Brick::Effect(-1):
	{
		ball.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x * 1.3, ball.GetBallDirection().y * 1.3));
		break;
	}
	case Brick::Effect(3):
	{
		ball.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x * 0.7, ball.GetBallDirection().y * 0.7));
		break;
	}
	case Brick::Effect(1):
	{
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(player.GetPaddle().GetLenght(), player.GetPaddle().GetHeight() * 1.2));
		}
		break;
	}
	case Brick::Effect(-2):
	{
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(player.GetPaddle().GetLenght(), player.GetPaddle().GetHeight() * 0.8));
		}
		break;
	}
	case Brick::Effect(2):
	{
		if (&player != nullptr)
		{
			player.IncreaseSpeed();
		}
		break;
	}
	case Brick::Effect(-3):
	{
		if (&player != nullptr)
		{
			player.DecreaseSpeed();
		}
		break;
	}
	case Brick::Effect(-4):
	{
		Ball new_ball(ball.GetX(), ball.GetY(), RadiusBall, ball.GetColor(), m_textures["Ball"]);
		new_ball.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x, -ball.GetBallDirection().y));
		m_balls.push_back(new_ball);
		break;
	}
	default:
		break;
	}
}

void Pong::Update(float delta)
{
	PlayerInput(delta);

	if (m_nrPlayers == 1)
	{
		AIPaddleMovement(delta);
	}

	WindowColision();
	BrickColision();
	PaddleColison();

	for (Ball& ball : m_balls)
	{
		ball.Move(delta);
	}
}

void Pong::InitializeGame()
{
	std::vector<Brick> bricks;
	std::random_device rand;

	std::uniform_int_distribution<int> distribAdvantages(1, 3);
	std::uniform_int_distribution<int> distribDisadvantages(-4, -1);
	std::uniform_int_distribution<int> distribDurability(2, 3);
	std::uniform_int_distribution<int> randomDirection(0, 1);

	int randBallXDirection = randomDirection(rand);
	int randBallYDirection = randomDirection(rand);

	int nrAdvantages = 2 + m_difficulty;
	int nrDisadvantages = 2 + m_difficulty;
	int nrToughBricks = 2 + m_difficulty * 2;
	int nrBricks = 5 + m_difficulty * 3;

	int coordX = 0;
	int coordY = 0;
	int space = 20;

	const int brickWidth = 35;
	const int brickHeight = 95;

	int column = WindowWidth / (brickWidth + (space / 2) * 2);
	int row = WindowHeight / (brickHeight + space * 2);

	std::uniform_int_distribution<int> distribX(8, column - 9);
	std::uniform_int_distribution<int> distribY(0, row - 1);

	int currentNrAdvantages = 0;
	int currentNrDisadvantages = 0;
	int curentNrToughBricks = 0;

	int index = 0;

	while (index < nrBricks)
	{
		coordX = distribX(rand) * (brickWidth + (space / 2) * 2);
		coordY = distribY(rand) * (brickHeight + space * 2);

		Brick::Effect effect = Brick::Effect(0);
		int durability = 1;

		if (curentNrToughBricks++ < nrToughBricks)
		{
			durability = distribDurability(rand);
		}

		if (currentNrAdvantages++ < nrAdvantages)
		{
			effect = Brick::Effect(distribDisadvantages(rand));
		}
		else if (currentNrDisadvantages++ < nrDisadvantages)
		{
			effect = Brick::Effect(distribDisadvantages(rand));
		}

		Brick brick(sf::Vector2f(coordX + space / 2, coordY + space), sf::Vector2f(brickWidth, brickHeight), durability, CustomRed, effect, m_textures["Brick"]);

		if (!IsBrickOverlapping(brick, bricks))
		{
			if (brick.EffectToString() != "")
			{
				brick.SetEffectTexture(m_textures[brick.EffectToString()]);
			}
			brick.UpdateEffectPosition();
			bricks.push_back(brick);
			index++;
		}
		else
		{
			currentNrDisadvantages--;
			currentNrAdvantages--;
			curentNrToughBricks--;
		}
	}

	for (Brick& brick : bricks)
	{
		this->m_bricks[brick.GetID()] = brick;
	}

	for (Ball& ball : m_balls)
	{
		ball.SetBallDirection(sf::Vector2f(InitialBallSpeed - randBallXDirection * 2 * InitialBallSpeed, InitialBallSpeed - randBallYDirection * 2 * InitialBallSpeed));
	}

	if (m_bricks.empty() || m_balls.empty() || m_players.size() != 2)
	{
		Logger::log("Intialization for Pong failed", Logger::Level::Error);
	}
	else
	{
		Logger::log("Intialization for Pong Complete", Logger::Level::Info);
	}
}

void Pong::WindowColision()
{
	for (Ball& m_ball : m_balls)
	{
		if (m_ball.GetY() + m_ball.GetRadius() >= WindowHeight)
		{
			m_sounds["BallBounce"].play();
			m_ball.SetPosition(m_ball.GetX(), WindowHeight - m_ball.GetRadius() - 1.0f);
			m_ball.SetBallDirection(sf::Vector2f(m_ball.GetBallDirection().x, -m_ball.GetBallDirection().y));
		}
		if (m_ball.GetX() + m_ball.GetRadius() >= WindowWidth)
		{
			m_ball.SetPosition(WindowWidth - m_ball.GetRadius() - 1.0f, m_ball.GetY());
			BallWindowCollision(m_ball);
		}
		if (m_ball.GetY() <= 0)
		{
			m_sounds["BallBounce"].play();
			m_ball.SetPosition(m_ball.GetX(), 1.0f);
			m_ball.SetBallDirection(sf::Vector2f(m_ball.GetBallDirection().x, -m_ball.GetBallDirection().y));

		}
		if (m_ball.GetX() <= 0)
		{
			m_ball.SetPosition(1.0f, m_ball.GetY());
			BallWindowCollision(m_ball);
		}
	}
}

void Pong::PlayerInput(float delta)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		if (m_players[0].GetPaddle().GetY() < WindowHeight - m_players[0].GetPaddle().GetHeight())
		{
			m_players[0].Move(sf::Vector2f(0, delta * m_players[0].GetSpeed()));
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		if (m_players[0].GetPaddle().GetY() > 0)
		{
			m_players[0].Move(sf::Vector2f(0, -(delta * m_players[0].GetSpeed())));
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && m_nrPlayers == 2)
	{
		if (m_players[1].GetPaddle().GetY() < WindowHeight - m_players[1].GetPaddle().GetHeight())
		{
			m_players[1].Move(sf::Vector2f(0, delta * m_players[1].GetSpeed()));
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && m_nrPlayers == 2)
	{
		if (m_players[1].GetPaddle().GetY() > 0)
		{
			m_players[1].Move(sf::Vector2f(0, -(delta * m_players[1].GetSpeed())));
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		m_gameState = GameState::Pause;
	}
}

void Pong::CheckGameWon()
{
	for (auto& player : m_players)
	{
		if (player.GetLives() == 0)
		{
			player.SetAsLoser();
			m_gameState = GameState::GameOver;
		}
	}
}

void Pong::SetGameOver(sf::Text& score, sf::Text& winner)
{
	if (m_nrPlayers == 2)
	{
		m_sounds["Victory"].play();
	}
	else
	{
		if (m_players[0].IsWinner())
		{
			m_sounds["Victory"].play();
		}
		else
		{
			m_sounds["Fail"].play();
		}
	}
	for (auto player : m_players)
	{
		if (player.IsWinner())
		{
			winner.setString("The winner is: " + player.GetName());
			score.setString(" The score is: " + std::to_string(player.GetScore()));
			break;
		}
	}
}

void Pong::AIPaddleMovement(float delta)
{
	if (m_balls[0].GetX() < WindowWidth/3 && timer.getElapsedTime() > sf::milliseconds(5))
	{
		if ((m_balls[0].GetY() > m_players[1].GetPaddle().GetY() + m_players[1].GetPaddle().GetHeight() / 2) && (m_players[1].GetPaddle().GetY() < WindowHeight - m_players[1].GetPaddle().GetHeight()))
		{  
			// If the ball is below the center of the paddle
			m_players[1].SetSpeed(std::abs(m_players[1].GetSpeed()));
		}
		else if ((m_balls[0].GetY() < m_players[1].GetPaddle().GetY() + m_players[1].GetPaddle().GetHeight()) && (m_players[1].GetPaddle().GetY() > 0))
		{  
			// If the ball is above the center of the paddle
			m_players[1].SetSpeed(-std::abs(m_players[1].GetSpeed()));
		}
		m_players[1].Move(sf::Vector2f(0, delta * (m_players[1].GetSpeed())));
		timer.restart();
	}
}

void Pong::CalculateScore(const Brick& brick, Player& player)
{
	player.AddPointsToScore(0);
}

void Pong::BallPaddleCollision(Ball& ball, Paddle& paddle, float testX, float testY)
{
	float ballCenterX = ball.GetX() + ball.GetRadius();
	float ballCenterY = ball.GetY() + ball.GetRadius();

	sf::FloatRect paddleBounds = paddle.GetGlobalBounds();
	float delta, ratio;

	ball.SetColor(paddle.GetColor());

	delta = (ballCenterX)-(paddleBounds.top + paddleBounds.height / 2);
	ratio = std::abs(delta) / (paddleBounds.height / 2);

	float speed = std::abs(ball.GetBallDirection().x) + std::abs(ball.GetBallDirection().y);
	if (ratio > 0.65f) ratio = 0.65f;
	if (ratio < 0.2f) ratio = 0.2f;

	float x = ratio * speed;
	float y = (1 - ratio) * speed;

	if (ball.GetBallDirection().y <= 0) y = -y;
	if (paddle.GetX() > BallCoordX) x = -x;
	sf::Vector2f direction(x, y);
	ball.SetBallDirection(direction);

	if (testY == paddleBounds.top)
	{
		ball.SetPosition(ball.GetX(), paddleBounds.top - ball.GetRadius() * 2 - 1);
	}
	if (testX == paddleBounds.left || testX == paddleBounds.left + paddleBounds.width)
	{
		if (testX == paddleBounds.left)
		{
			ball.SetPosition(paddleBounds.left - ball.GetRadius() * 2 - 1, ball.GetY());
		}
		else if (testX == paddleBounds.left + paddleBounds.width)
		{
			ball.SetPosition(paddleBounds.left + paddleBounds.width + 1, ball.GetY());
		}
	}
}

void Pong::BallBrickCollsision(Ball& ball, Brick& brick, float testX, float testY)
{
	sf::FloatRect brickBounds = brick.GetGlobalBounds();
	int playerIndex = -1;
	for (int index = 0; index < m_players.size(); ++index)
	{
		if (ball.GetColor() == m_players[index].GetPaddle().GetColor())
		{
			playerIndex = index;
		}
	}
	if (playerIndex != -1)
	{
		CalculateScore(brick, m_players[playerIndex]);
	}

	brick.DecreaseDurability();
	if (brick.GetEffect() == Brick::Effect(-4))
	{
		if (brick.GetDurability() == 0)
		{
			ApplyEffect(brick.GetEffect(), ball, m_players[playerIndex]);
			brick.SetEffect(Brick::Effect(0));
		}
	}
	else
	{
		if (m_effects.find(brick.GetEffect()) == m_effects.end())
		{
			m_effects[brick.GetEffect()] = true;
			ApplyEffect(brick.GetEffect(), ball, m_players[playerIndex]);
			std::thread newThread(&Game::ResetEffect, this, std::ref(ball), std::ref(m_players[playerIndex]), brick.GetEffect());
			newThread.detach();
			brick.SetEffect(Brick::Effect(0));
		}
	}

	if (testY == brickBounds.top || testY == brickBounds.top + brickBounds.height)
	{
		sf::Vector2f direction(ball.GetBallDirection().x, -ball.GetBallDirection().y);
		ball.SetBallDirection(direction);
		if (testY == brickBounds.top)
		{
			ball.SetPosition(ball.GetX(), brickBounds.top - ball.GetRadius() * 2 - 1);
		}
		else if (testY == brickBounds.top + brickBounds.height)
		{
			ball.SetPosition(ball.GetX(), brickBounds.top + brickBounds.height + 1);
		}
	}
	else if (testX == brickBounds.left || testX == brickBounds.left + brickBounds.width)
	{
		sf::Vector2f direction(-ball.GetBallDirection().x, ball.GetBallDirection().y);
		ball.SetBallDirection(direction);
		if (testX == brickBounds.left)
		{
			ball.SetPosition(brickBounds.left - ball.GetRadius() * 2 - 1, ball.GetY());
		}
		else if (testX == brickBounds.left + brickBounds.width)
		{
			ball.SetPosition(brickBounds.left + brickBounds.width + 1, ball.GetY());
		}
	}
}

sf::Vector2f Pong::ResetBallDirection(const Ball& ball)
{
	float ratio = std::abs(ball.GetBallDirection().x) / (std::abs(ball.GetBallDirection().x) + std::abs(ball.GetBallDirection().y));
	return sf::Vector2f(ratio * InitialBallSpeed * 2 * (ball.GetBallDirection().x / std::abs(ball.GetBallDirection().x)),
		(1 - ratio) * InitialBallSpeed * 2 * (ball.GetBallDirection().y / std::abs(ball.GetBallDirection().y)));
}

bool Pong::IsBrickOverlapping(const Brick& brick, std::vector<Brick> bricks)
{
	for (const Brick& it : bricks)
	{
		if (it.GetGlobalBounds().intersects(brick.GetGlobalBounds()))
		{
			return true;
		}
	}

	if (brick.GetGlobalBounds().intersects(m_balls[0].GetGlobalBounds()))
	{
		return true;
	}

	return false;
}
