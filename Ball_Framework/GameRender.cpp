#include "GameRender.h"
#include <iostream>
#include "../Logger/Logging.h"

GameRender::GameRender(std::shared_ptr<OurWindow> window)
{
	this->m_window = window;
	m_button_texture.loadFromFile("../Resources/Button.jpg");
	m_background_texture.loadFromFile("../Resources/Background.jpg");
	m_background.setTexture(m_background_texture);
	m_background.setScale(1 + (WindowWidth - m_background.getGlobalBounds().width) / m_background.getGlobalBounds().width, 1 + (WindowHeight - m_background.getGlobalBounds().height) / m_background.getGlobalBounds().height);
	m_font.loadFromFile("../Resources/arial.ttf");

	m_score.setFont(m_font);
	m_winner_text.setFont(m_font);
	m_score.setCharacterSize(31);
	m_winner_text.setCharacterSize(31);
	m_score.setFillColor(sf::Color::White);
	m_winner_text.setFillColor(sf::Color::White);
}

void GameRender::Render(Game& game)
{
	m_window->GetWindow().clear();
	m_window->GetWindow().draw(game.m_background);
	for (Player& player : game.GetPlayers())
	{
		m_window->GetWindow().draw(player.GetPaddle());
		m_window->GetWindow().draw(*player.GetText());
		m_window->GetWindow().draw(player.GetHearts());
	}
	for (const Ball& ball : game.GetBalls())
	{
		m_window->GetWindow().draw(ball);
	}

	for (const auto& brick : game.GetBricks())
	{
		m_window->GetWindow().draw(brick.second);
	}
	m_window->GetWindow().display();
}

void GameRender::GameOver(Game& game)
{
	Logger::log("Game finished", Logger::Level::Info);
	m_statistic.UpdateStatisitics(game);

	constexpr float space = 35;
	uint8_t option = -1;
	std::array<Button, 3> buttons;
	std::array<std::string, 3> buttonNames{ "Main Menu",  "Restart" ,  "Exit" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, WindowHeight / buttons.size() + index * (buttons[index].GetGlobalBounds().height + space)));
		buttons[index].SetFont(m_font);
	}

	game.SetGameOver(m_score, m_winner_text);

	if (m_winner_text.getString() == "")
	{
		Logger::log("The winner has no name", Logger::Level::Warning);
	}

	m_winner_text.setPosition(sf::Vector2f(WindowWidth / 2 - m_winner_text.getGlobalBounds().width / 2, WindowHeight / 11));
	m_score.setPosition(sf::Vector2f(WindowWidth / 2 - m_score.getGlobalBounds().width / 2, WindowHeight / 10 + WindowHeight / 10));

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
				if (buttons[index].IsPressed())
				{
					option = index;
				}
			}

			m_window->ClosingWindow(event);
		}

		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		m_window->GetWindow().draw(m_score);
		m_window->GetWindow().draw(m_winner_text);
		for (auto& button : buttons)
		{
			m_window->GetWindow().draw(button);
		}
		m_window->GetWindow().display();


		if (option == 0)
		{
			break;
		}
		if (option == 1)
		{
			game.Restart();
			break;
		}
		if (option == 2)
		{
			m_window->GetWindow().close();
		}
	}
}

void GameRender::PauseMenu(Game& game)
{
	constexpr float space = 35;
	uint8_t option = 0;
	std::array<Button, 4> buttons;
	std::array<std::string, 4> buttonNames{ "Continue",  "Restart" ,  "Main Menu" ,  "Exit" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, (WindowHeight / 2 - buttons.size() * (buttons[index].GetGlobalBounds().height + space) / 2) + index * (buttons[index].GetGlobalBounds().height + space))));
		buttons[index].SetFont(m_font);
	}

	Button mute = Button("Mute", 20, sf::Color::Black, m_button_texture);
	mute.SetPosition(sf::Vector2f(WindowWidth - mute.GetGlobalBounds().width - space, space));
	mute.SetFont(m_font);

	Button unmute = Button("Unmute", 20, sf::Color::Black, m_button_texture);
	unmute.SetPosition(sf::Vector2f(WindowWidth - unmute.GetGlobalBounds().width - space, mute.GetGlobalBounds().top + mute.GetGlobalBounds().height + space));
	unmute.SetFont(m_font);

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;

		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
			}
			mute.UpdateState(event, m_window->GetWindow());
			unmute.UpdateState(event, m_window->GetWindow());

			if (buttons[0].IsPressed())
			{
				game.SetGameState(Game::GameState::Playing);
				option = 1;
			}
			else if (buttons[1].IsPressed())
			{
				game.SetGameState(Game::GameState::Playing);
				game.Restart();
				option = 1;
			}
			else if (buttons[2].IsPressed())
			{
				option = 1;
				game.SetGameState(Game::GameState::GameInterrupted);
			}
			else if (buttons[3].IsPressed())
			{
				m_window->GetWindow().close();
			}
			else if (mute.IsPressed())
			{
				for (auto& sound : game.GetSounds())
				{
					sound.second.setVolume(0);
				}
			}
			else if (unmute.IsPressed())
			{
				for (auto& sound : game.GetSounds())
				{
					sound.second.setVolume(50);
				}
			}

			m_window->ClosingWindow(event);
		}
		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		m_window->GetWindow().draw(mute);
		m_window->GetWindow().draw(unmute);
		for (auto& button : buttons)
		{
			m_window->GetWindow().draw(button);
		}

		m_window->GetWindow().display();
		if (option == 1)
		{
			break;
		}
	}
}

OurWindow& GameRender::GetWindow()
{
	return *m_window;
}

