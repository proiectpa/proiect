#include <SFML/Graphics.hpp>
#include <iostream>
#include "OurWindow.h"
#include "MainMenu.h"
#include "../Logger/Logging.h"

int main()
{
	Logger::log("Program started", Logger::Level::Info);

	MainMenu menu(std::make_shared<OurWindow>());
	menu.RunMenu();
	return 0;
}