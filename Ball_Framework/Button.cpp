#include "Button.h"
#include <iostream>

Button::Button(std::string text, int characterSize,sf::Color textColor, sf::Texture& texture)
{
	m_text.setString(text);
	m_text.setFillColor(textColor);
	m_text.setCharacterSize(characterSize);
	m_button.setTexture(texture);
	m_pressed = false;
	m_mouseOver = false;
}

void Button::SetPosition(sf::Vector2f position)
{
	m_button.setPosition(position);
	float xPosition = (position.x + m_button.getGlobalBounds().width / 9) - (m_text.getGlobalBounds().width / 2);
	float yPosition = (position.y + m_button.getGlobalBounds().height / 4) - (m_text.getGlobalBounds().height / 2);
	m_text.setPosition({ xPosition,yPosition });
}

void Button::SetFont(sf::Font& font)
{
	m_text.setFont(font);
}

void Button::SetTint(sf::Color color)
{
	m_button.setColor(color);
}

void Button::UpdateState(sf::Event& event, sf::RenderWindow& window)
{
	if (this->IsMouseOver(window))
	{
		this->SetTint(sf::Color(200, 200, 255, 255));
		this->m_mouseOver = true;
	}
	else
	{
		this->SetTint(sf::Color::White);
		this->m_mouseOver = false;
	}

	if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left && m_mouseOver)
	{
		this->m_pressed = true;
	}
	else
	{
		this->m_pressed = false;
	}
}


void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(this->m_button, states);
	target.draw(this->m_text, states);
}

bool Button::IsMouseOver(sf::RenderWindow& window)
{
	sf::FloatRect bounds = m_button.getGlobalBounds();
	if (bounds.contains(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y))
	{
		return true;
	}
	return false;
}

bool Button::IsPressed()
{
	return m_pressed;
}

const sf::FloatRect& Button::GetGlobalBounds() const
{
	return m_button.getGlobalBounds();
}
