#include "Paddle.h"

Paddle::Paddle(sf::Vector2f position, sf::Vector2f dimension, sf::Texture& texture)
{
	m_paddle.setPosition(position);
	m_paddle.setSize(dimension);
	m_paddle.setTexture(&texture);
}

Paddle::~Paddle()
{
}

void Paddle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(this->m_paddle, states);
}

void Paddle::SetPosition(sf::Vector2f position)
{
	m_paddle.setPosition(position);
}

void Paddle::SetDimension(sf::Vector2f dimension)
{
	m_paddle.setSize(dimension);
}

void Paddle::SetColor(const sf::Color& color)
{
	this->m_color = color;
}

void Paddle::MovePaddle(sf::Vector2f vector)
{
	m_paddle.move(vector);
}

const float& Paddle::GetX() const
{
	return m_paddle.getPosition().x;
}

const float& Paddle::GetY() const
{
	return m_paddle.getPosition().y;
}

const float& Paddle::GetHeight() const
{
	return m_paddle.getSize().y;
}

const float& Paddle::GetLenght() const
{
	return m_paddle.getSize().x;
}

sf::FloatRect Paddle::GetGlobalBounds()
{
	return m_paddle.getGlobalBounds();
}

const sf::Color& Paddle::GetColor() const
{
	return this->m_color;
}
