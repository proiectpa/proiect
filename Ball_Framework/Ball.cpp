#include "Ball.h"

Ball::Ball(float x, float y, float radius, sf::Color color, sf::Texture& texture) :
	m_radius(radius),
	m_ball(texture)
{
	m_ballDirection.x = 0;
	m_ballDirection.y = 0;
	m_ball.setPosition(x, y);
	SetColor(color);
}

void Ball::SetPosition(float x, float y)
{
	this->m_ball.setPosition(x, y);
}

void Ball::Move(float delta)
{
	this->m_ball.move(sf::Vector2f(m_ballDirection.x*delta,m_ballDirection.y*delta));
}

const float& Ball::GetX() const
{
	return m_ball.getPosition().x;
}

const float& Ball::GetY() const
{
	return m_ball.getPosition().y;
}

const sf::Vector2f& Ball::GetBallDirection() const
{
	return m_ballDirection;
}

const sf::FloatRect& Ball::GetGlobalBounds() const
{
	return m_ball.getGlobalBounds();
}

const uint16_t& Ball::GetRadius() const
{
	return m_radius;
}

const sf::Color& Ball::GetColor() const
{
	return m_color;
}

void Ball::SetColor(sf::Color color)
{
	this->m_color = color;
	this->m_ball.setColor(color);
}

void Ball::SetBallDirection(sf::Vector2f direction)
{
	m_ballDirection.x = direction.x;
	m_ballDirection.y = direction.y;
}

void Ball::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(this->m_ball, states);
}

