#include "Brick.h"

Brick::Brick(sf::Vector2f position, sf::Vector2f dimension, uint8_t durability, sf::Color color, Effect effect, sf::Texture& texture):
	m_color(color),
	m_durability(durability),
	m_effect(effect)
{
	m_brick.setSize(dimension);
	m_brick.setPosition(position);
	m_effectSprite.setPosition(position);
	m_brick.setFillColor(color);
	m_brick.setTexture(&texture);
	GenerateID();
}
const char* Brick::EffectToString()
{
	switch (m_effect)
	{
	case Effect::NoEffect:
		return "NoEffect";
	case Effect::BallDecreaseSpeed:
		return "BallDecreaseSpeed";
	case Effect::BallIncreaseSpeed:
		return "BallIncreaseSpeed";
	case Effect::PaddleDecreaseSize:
		return "PaddleDecreaseSize";
	case Effect::PaddleIncreaseSize:
		return "PaddleIncreaseSize";
	case Effect::PaddleDecreaseSpeed:
		return "PaddleDecreaseSpeed";
	case Effect::PaddleIncreaseSpeed:
		return "PaddleIncreaseSpeed";
	case Effect::BrickBecomesBall:
		return "BrickBecomesBall";
	default:
		return "";
	}
}

void Brick::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(this->m_brick,states);
	target.draw(this->m_effectSprite, states);
}

void Brick::SetPosition(float coordX, float coordY)
{
	this->m_brick.setPosition(coordX, coordY);
}

void Brick::UpdateEffectPosition()
{
	m_effectSprite.setPosition(m_brick.getPosition().x + m_brick.getGlobalBounds().width / 2 - m_effectSprite.getGlobalBounds().width / 2,
							   m_brick.getPosition().y + m_brick.getGlobalBounds().height / 2 - m_effectSprite.getGlobalBounds().height / 2);
}

void Brick::SetColor(sf::Color color)
{
	this->m_color = color;
	this->m_brick.setFillColor(color);
}

void Brick::SetTexture(sf::Texture& texture)
{
	this->m_brick.setTexture(&texture);
}

void Brick::SetEffectTexture(sf::Texture& texture)
{
	m_effectSprite.setTexture(texture);
	m_effectSprite.setScale(1.5, 1.5);
}

void Brick::SetEffect(Brick::Effect effect)
{
	m_effect = effect;
	m_effectSprite.setTextureRect(sf::IntRect(0,0,0,0));
}

void Brick::DecreaseDurability()
{
	m_durability--;
}

void Brick::SetSizeBrick(sf::Vector2f vector)
{
	this->m_brick.setSize(vector);
}

const float& Brick::GetCoordX() const
{
	return m_brick.getPosition().x;
}

const float& Brick::GetCoordY() const
{
	return m_brick.getPosition().y;
}

const Brick::Effect& Brick::GetEffect() const
{
	return m_effect;
}

const sf::FloatRect Brick::GetGlobalBounds() const
{
	return m_brick.getGlobalBounds();
}

void Brick::GenerateID()
{
	this->m_ID = "";
	m_ID +=std::to_string(GetCoordX())+'x';
	m_ID += std::to_string(GetCoordY()) + 'y';
}

const std::string& Brick::GetID() const
{
	return m_ID;
}

const uint8_t& Brick::GetDurability() const
{
	return m_durability;
}

const sf::Color& Brick::GetColor() const
{
	return m_color;
}