#pragma once
#include <SFML/Graphics.hpp>
#include <sstream>

#define DELETE_KEY 8
#define ENTER_KEY 13
#define ESCAPE_KEY 27

class Textbox:public sf::Drawable
{
public:
	Textbox() = default;
	Textbox(int size, sf::Color color, bool initialSelectedState);

	void SetFont(sf::Font& font);
	void SetPosition(sf::Vector2f position);
	void SetLimit(bool TrueOrFalse);
	void SetLimit(bool TrueOrFalse, int limit);
	void SetText(const std::string& text);
	void SetAttribute(const std::string& attribute);
	void SetColor(const sf::Color& color);

	std::string GetText();
	const sf::Text& GetAttribute() const;
	const sf::FloatRect& GetGlobalBounds() const;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void Selected(bool selected);
	void TypedOn(sf::Event input);
	bool IsMouseOver(sf::RenderWindow& window);

private:
	sf::Text textbox;
	sf::Text m_attribute;
	std::ostringstream text;
	sf::RectangleShape m_rectangle;
	bool isSelected=false;
	bool hasLimit=false;
	int limit;

	void InputLogic(int charTyped);
	void DeleteLastCharacter();
	
	



};

