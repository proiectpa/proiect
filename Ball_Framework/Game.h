#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <unordered_map>
#include <random>
#include "OurWindow.h"
#include "Player.h"
#include "Ball.h"
#include "Brick.h"

const float PaddleLenght = 100;
const float PaddleHeight = 20;
const float InitialBallSpeed = 260;
const float InitialPaddleSpeed = 550;
const float RadiusBall = 10;
const float BallCoordX = WindowWidth / 2;
const float BallCoordY = WindowHeight / 2;

const sf::Color CustomRed(255, 100, 100, 255);
const sf::Color CustomBlue(100, 100, 255, 255);
const sf::Color CustomNeutralColor(200, 200, 200, 255);

class GameRender;

class Game
{
public:
	enum class GameType
	{
		Pong,
		BrickBreaker
	};
	enum class GameState
	{
		Playing,
		Pause,
		GameOver,
		GameInterrupted
	};

	enum class MultiplayerType
	{
		Collaborator,
		Oponent,
		HumanOponent,
		AiOponent
	};

	Game(int difficulty);
	~Game();

	virtual void Run(GameRender& render);
	virtual void Restart()=0;
	virtual void ResetEffect(Ball& ball, Player& player, Brick::Effect effect)=0;

	virtual void SetGameOver(sf::Text& score,sf::Text& winner) = 0;
	void SetGameState(GameState state);

	void BallMovementDelay(Ball& ball);

	std::vector<Player>& GetPlayers();
	std::vector<Ball>& GetBalls();
	sf::Sprite m_background;
	std::unordered_map<std::string, Brick>& GetBricks();
	std::unordered_map<std::string, sf::Sound>& GetSounds();
	const Game::GameType& GetGameType()const;
	const MultiplayerType& GetMultiplayerType()const;

protected:
	virtual void Update(float delta) = 0;
	virtual void InitializeGame() = 0;
	virtual void PlayerInput(float delta) = 0;
	virtual void WindowColision() = 0;
	virtual void BallWindowCollision(Ball& ball)=0;
	virtual void CalculateScore(const Brick& brick, Player& player)=0;
	virtual void ApplyEffect(Brick::Effect effect, Ball& ball, Player& player)=0;
	virtual void BallPaddleCollision(Ball& ball, Paddle& paddle, float testX, float testY) = 0;
	virtual void BallBrickCollsision(Ball& ball, Brick& brick, float testX, float testY) = 0;
	virtual void CheckGameWon() = 0;
	virtual sf::Vector2f ResetBallDirection(const Ball& ball) = 0;

	void BrickColision();
	void PaddleColison();
protected:
	GameType m_gameType;
	uint8_t m_difficulty;
	GameState m_gameState;
	MultiplayerType m_multiplayerType;
	sf::Font m_scoreFont;

	std::vector<Player> m_players;
	std::vector<Ball> m_balls;
	std::unordered_map<std::string, Brick> m_bricks;
	std::unordered_map<Brick::Effect, bool> m_effects;
	std::unordered_map<std::string, sf::Texture> m_textures;
	std::unordered_map<std::string, sf::SoundBuffer> m_soundBuffers;
	std::unordered_map<std::string, sf::Sound> m_sounds;
};

