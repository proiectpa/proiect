#pragma once
#include "SFML\Graphics.hpp"
#include "Player.h"

class Ball : public sf::Drawable
{
public:
	Ball(float x, float y, float radius, sf::Color, sf::Texture& texture);
	Ball() = default;

	void SetPosition(float x, float y);
	void SetColor(sf::Color color);
	void SetBallDirection(sf::Vector2f direction);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void Move(float delta);

	const float& GetX() const;
	const float& GetY() const;
	const sf::Vector2f& GetBallDirection() const;
	const sf::FloatRect& GetGlobalBounds() const;
	const uint16_t& GetRadius() const;
	const sf::Color& GetColor() const;

private:
	sf::Color m_color;
	sf::Sprite m_ball;
	float m_radius;
	sf::Vector2f m_ballDirection;
};