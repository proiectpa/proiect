#include "MainMenu.h"
#include <time.h>
#include <iostream>

MainMenu::MainMenu(std::shared_ptr<OurWindow> window)
{
	m_window = window;
	m_difficulty = 0;
	m_nrPlayers = 0;
	m_game_type = "";
	m_multiplayer_type;
	m_player1 = "Player1";
	m_player2 = "Player2";
	m_font.loadFromFile("../Resources/arial.ttf");
	m_button_texture.loadFromFile("../Resources/Button.jpg");
	m_previous = Button("Previous", 20, sf::Color::Black, m_button_texture);
	m_previous.SetPosition(sf::Vector2f(10, WindowHeight - m_previous.GetGlobalBounds().height - 10));
	m_previous.SetFont(m_font);

	m_instructions = Button("Help", 20, sf::Color::Black, m_button_texture);
	m_instructions.SetPosition(sf::Vector2f(WindowWidth - m_instructions.GetGlobalBounds().width - 10, 10));
	m_instructions.SetFont(m_font);

	m_background_texture.loadFromFile("../Resources/Background.jpg");
	m_background.setTexture(m_background_texture);
	m_background.setScale(1 + (WindowWidth - m_background.getGlobalBounds().width) / m_background.getGlobalBounds().width, 1 + (WindowHeight - m_background.getGlobalBounds().height) / m_background.getGlobalBounds().height);

	m_instructions_texture.loadFromFile("../Resources/Instructions.jpeg");
	m_background_instructions.setTexture(m_instructions_texture);
	m_background_instructions.setScale(1 + (WindowWidth - m_background_instructions.getGlobalBounds().width) / m_background_instructions.getGlobalBounds().width, 1 + (WindowHeight - m_background_instructions.getGlobalBounds().height) / m_background_instructions.getGlobalBounds().height);
}

void MainMenu::ChooseGame()
{
	m_game_type = "";
	constexpr float space = 35;
	std::array<Button, 2> buttons;
	std::array<std::string, 2> buttonNames{ "Brick Breaker",  "Pong" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, (WindowHeight / 2 - buttons.size() * (buttons[index].GetGlobalBounds().height + space) / 2) + index * (buttons[index].GetGlobalBounds().height + space))));
		buttons[index].SetFont(m_font);
	}

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
				if (buttons[index].IsPressed())
				{
					m_game_type = buttonNames[index];
				}
			}
			m_instructions.UpdateState(event, m_window->GetWindow());
			if (m_instructions.IsPressed())
			{
				Instructions();
			}
			m_window->ClosingWindow(event);
		}

		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		m_window->GetWindow().draw(m_instructions);
		for (auto& button : buttons)
		{
			m_window->GetWindow().draw(button);
		}
		m_window->GetWindow().display();

		if (m_game_type != "")
		{
			break;
		}
	}
}

void MainMenu::ChooseDifficulty()
{
	m_difficulty = 0;
	constexpr float space = 35;
	std::array<Button, 3> buttons;
	std::array<std::string, 3> buttonNames{ "Easy",  "Medium", "Hard" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, (WindowHeight / 2 - buttons.size() * (buttons[index].GetGlobalBounds().height + space) / 2) + index * (buttons[index].GetGlobalBounds().height + space))));
		buttons[index].SetFont(m_font);
	}

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
				if (buttons[index].IsPressed())
				{
					m_difficulty = index + 1;
					m_changedNrPlayers = true;
				}
			}
			m_instructions.UpdateState(event, m_window->GetWindow());
			m_previous.UpdateState(event, m_window->GetWindow());
			if (m_previous.IsPressed())
			{
				ChooseGame();
			}
			if (m_instructions.IsPressed())
			{
				Instructions();
			}
			m_window->ClosingWindow(event);
		}

		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		for (auto& button : buttons)
		{
			m_window->GetWindow().draw(button);
		}
		m_window->GetWindow().draw(m_previous);
		m_window->GetWindow().draw(m_instructions);
		m_window->GetWindow().display();
		if (m_difficulty != 0)
		{
			break;
		}
	}
}

void MainMenu::ChooseNrPlayers()
{
	m_nrPlayers = 0;
	constexpr float space = 35;
	std::array<Button, 2> buttons;
	std::array<std::string, 2> buttonNames{ "1 Player",  "2 Players" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, (WindowHeight / 2 - buttons.size() * (buttons[index].GetGlobalBounds().height + space) / 2) + index * (buttons[index].GetGlobalBounds().height + space))));
		buttons[index].SetFont(m_font);
	}

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
				if (buttons[index].IsPressed())
				{
					m_nrPlayers = index + 1;
				}
			}
			m_instructions.UpdateState(event, m_window->GetWindow());
			m_previous.UpdateState(event, m_window->GetWindow());
			if (m_previous.IsPressed())
			{
				ChooseDifficulty();
			}
			if (m_instructions.IsPressed())
			{
				Instructions();
			}
			m_window->ClosingWindow(event);
		}

		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		for (auto& button : buttons)
		{
			m_window->GetWindow().draw(button);
		}
		m_window->GetWindow().draw(m_previous);
		m_window->GetWindow().draw(m_instructions);
		m_window->GetWindow().display();
		if (m_nrPlayers != 0)
		{
			m_changedNrPlayers = true;
			break;
		}
	}
}

void MainMenu::ChooseMultiplayerType()
{
	constexpr float space = 35;
	int option = -1;
	std::array<Button, 2> buttons;
	std::array<std::string, 2> buttonNames{ "Collaborator",  "Oponent" };

	for (int index = 0; index < buttonNames.size(); ++index)
	{
		buttons[index] = Button(buttonNames[index], 20, sf::Color::Black, m_button_texture);
		buttons[index].SetPosition(sf::Vector2f(WindowWidth / 2 - buttons[index].GetGlobalBounds().width / 2, (WindowHeight / 2 - buttons.size() * (buttons[index].GetGlobalBounds().height + space) / 2) + index * (buttons[index].GetGlobalBounds().height + space)));
		buttons[index].SetFont(m_font);
	}

	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			for (int index = 0; index < buttons.size(); ++index)
			{
				buttons[index].UpdateState(event, m_window->GetWindow());
				if (buttons[index].IsPressed())
				{
					option = index;
				}
			}
			m_instructions.UpdateState(event, m_window->GetWindow());
			m_previous.UpdateState(event, m_window->GetWindow());
			if (m_previous.IsPressed())
			{
				ChooseNrPlayers();
				m_changedNrPlayers = true;
			}
			if (m_instructions.IsPressed())
			{
				Instructions();
			}
			m_window->ClosingWindow(event);
		}

		if (m_nrPlayers == 2 && m_game_type == "Brick Breaker")
		{
			m_window->GetWindow().clear();
			m_window->GetWindow().draw(m_background);
			for (auto& button : buttons)
			{
				m_window->GetWindow().draw(button);
			}
			m_window->GetWindow().draw(m_previous);
			m_window->GetWindow().draw(m_instructions);
			m_window->GetWindow().display();
		}

		if (option == 0)
		{
			m_multiplayer_type = Game::MultiplayerType::Collaborator;
			m_changedNrPlayers = false;
			break;
		}
		if (option == 1)
		{
			m_multiplayer_type = Game::MultiplayerType::Oponent;
			m_changedNrPlayers = false;
			break;
		}
	}
}

void MainMenu::ChooseNames(bool& moveOn, bool& changedNrPlayers)
{
	m_player1 = "Player1";
	m_player2 = "Player2";
	moveOn = false;
	int nr = 0;
	bool canStart = false;
	Textbox input1;
	input1 = Textbox(30, sf::Color::White, true);
	input1.SetPosition(sf::Vector2f(WindowWidth / 2 - input1.GetGlobalBounds().width / 3, WindowHeight / 2 - input1.GetGlobalBounds().height));
	input1.SetFont(m_font);
	input1.SetAttribute("Player 1:");
	Textbox input2;
	Button start("Start", 20, sf::Color::Black, m_button_texture);
	start.SetPosition(sf::Vector2f(WindowWidth - start.GetGlobalBounds().width - 10, WindowHeight - start.GetGlobalBounds().height - 10));
	start.SetFont(m_font);
	bool valid = true;

	if (m_nrPlayers == 2)
	{
		input2 = Textbox(30, sf::Color::White, false);
		input2.SetPosition(sf::Vector2f(sf::Vector2f(WindowWidth / 2 - input2.GetGlobalBounds().width / 3, WindowHeight / 2 + input2.GetGlobalBounds().height + 35)));
		input2.SetFont(m_font);
		input2.SetAttribute("Player 2:");
	}
	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		if (sf::Event::MouseMoved)
		{
			if (m_previous.IsMouseOver(m_window->GetWindow()))
			{
				m_previous.SetTint(sf::Color(200, 200, 255, 255));
			}
			else
			{
				m_previous.SetTint(sf::Color::White);
			}

			if (m_instructions.IsMouseOver(m_window->GetWindow()))
			{
				m_instructions.SetTint(sf::Color(200, 200, 255, 255));
			}
			else
			{
				m_instructions.SetTint(sf::Color::White);
			}

			if (start.IsMouseOver(m_window->GetWindow()))
			{
				start.SetTint(sf::Color(200, 200, 255, 255));
			}
			else
			{
				start.SetTint(sf::Color::White);
			}

		}

		while (m_window->GetWindow().pollEvent(event))
		{
			m_window->ClosingWindow(event);
			switch (event.type)
			{
			case sf::Event::TextEntered:
				nr = 0;
				input1.TypedOn(event);
				if (std::regex_match(input1.GetText().data(), m_expresion))
				{
					++nr;
					input1.SetColor(sf::Color::White);
				}
				else
				{
					input1.SetColor(sf::Color::Red);
				}
				if (m_nrPlayers == 2)
				{
					input2.TypedOn(event);
					if (std::regex_match(input2.GetText().data(), m_expresion))
					{
						++nr;
						input2.SetColor(sf::Color::White);
					}
					else
					{
						input2.SetColor(sf::Color::Red);
					}
				}
				if (nr == m_nrPlayers)
				{
					valid = true;
				}
				else
				{
					valid = false;
				}
				break;
			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Left && input1.IsMouseOver(m_window->GetWindow()))
				{
					input2.Selected(false);
					input1.Selected(true);
				}
				if (event.mouseButton.button == sf::Mouse::Left && input2.IsMouseOver(m_window->GetWindow()))
				{
					input1.Selected(false);
					input2.Selected(true);
				}
				if (event.mouseButton.button == sf::Mouse::Left && m_previous.IsMouseOver(m_window->GetWindow()))
				{
					input1.SetText("");
					input2.SetText("");
					m_player1 = "Player1";
					m_player2 = "Player2";
					if (m_nrPlayers == 2 && m_game_type == "Brick Breaker")
					{
						ChooseMultiplayerType();
					}
					else
					{
						ChooseNrPlayers();
					}
				}
				if (valid == true && event.mouseButton.button == sf::Mouse::Left && start.IsMouseOver(m_window->GetWindow()))
				{
					if (input1.GetText() != "")
					{
						m_player1 = input1.GetText();
					}
					if (m_nrPlayers == 2 && input2.GetText() != "")
					{
						m_player2 = input2.GetText();
					}
					canStart = true;
				}
				if (event.mouseButton.button == sf::Mouse::Left && m_instructions.IsMouseOver(m_window->GetWindow()))
				{
					Instructions();
				}
				break;
			case sf::Event::KeyPressed:
			{
				if (valid == true && event.key.code == sf::Keyboard::Enter)
				{
					if (input1.GetText() != "")
					{
						m_player1 = input1.GetText();
					}
					if (m_nrPlayers == 2 && input2.GetText() != "")
					{
						m_player2 = input2.GetText();
					}
					canStart = true;
				}
				break;
			}
			break;
			default:
				break;
			}
		}
		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background);
		m_window->GetWindow().draw(input1);
		if (m_nrPlayers == 2)
		{
			m_window->GetWindow().draw(input2);
		}
		m_window->GetWindow().draw(m_previous);
		m_window->GetWindow().draw(m_instructions);
		m_window->GetWindow().draw(start);
		m_window->GetWindow().display();

		if (canStart)
		{
			moveOn = true;
			break;
		}
		if (changedNrPlayers)
		{
			break;
		}
	}
}

void MainMenu::Instructions()
{
	uint8_t option = 0;
	while (m_window->GetWindow().isOpen())
	{
		sf::Event event;
		while (m_window->GetWindow().pollEvent(event))
		{
			m_window->ClosingWindow(event);

			if (sf::Event::KeyPressed && event.key.code == sf::Keyboard::H)
			{
				option = 1;
			}
		}
		m_window->GetWindow().clear();
		m_window->GetWindow().draw(m_background_instructions);
		m_window->GetWindow().display();
		if (option == 1)
		{
			break;
		}
	}

}

void MainMenu::StartGame()
{
	if (m_game_type == "Brick Breaker")
	{
		if (m_nrPlayers == 1)
		{
			m_multiplayer_type = Game::MultiplayerType::Collaborator;
		}
		m_game = std::make_shared< BrickBreaker>(m_difficulty, m_nrPlayers, m_multiplayer_type, m_player1, m_player2);
	}
	else if (m_game_type == "Pong")
	{
		if (m_nrPlayers == 2)
		{
			m_multiplayer_type = Game::MultiplayerType::HumanOponent;
		}
		else
		{
			m_multiplayer_type = Game::MultiplayerType::AiOponent;
		}
		m_game = std::make_shared<Pong>(m_difficulty, m_nrPlayers, m_multiplayer_type, m_player1, m_player2);
	}

	GameRender render(m_window);
	m_game->Run(render);

}

void MainMenu::RunMenu()
{
	while (m_window->GetWindow().isOpen())
	{
		ChooseGame();
		ChooseDifficulty();
		bool moveOn = false;
		while (!moveOn)
		{
			if (m_changedNrPlayers)
			{
				ChooseNrPlayers();
				if (m_nrPlayers == 2 && m_game_type == "Brick Breaker")
				{
					ChooseMultiplayerType();
				}
				else
				{
					m_changedNrPlayers = false;
				}
			}
			ChooseNames(moveOn, m_changedNrPlayers);
			if (!m_window->GetWindow().isOpen())
			{
				break;
			}
		}
		if (m_game_type != "" && m_difficulty != 0 && m_nrPlayers != 0)
			StartGame();
	}
}
