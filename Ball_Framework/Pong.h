#pragma once
#include "Game.h"
#include"OurWindow.h"

const float InitialPaddleY_Pong = WindowHeight / 2 - PaddleLenght/2;
const float InitialLeftPaddleX_Pong = 3;
const float InitialRightPaddleX_Pong = WindowWidth - InitialLeftPaddleX_Pong - PaddleHeight;



class Pong : public Game
{
public:
	Pong(uint8_t difficulty, uint8_t nrPlayers, MultiplayerType multiplayer_type, std::string player1, std::string player2);

	void Restart() override;
	void BallWindowCollision(Ball& ball)override;
	void ResetEffect(Ball& ball, Player& player, Brick::Effect effect)override;
	void ApplyEffect(Brick::Effect effect, Ball& ball, Player& player)override;
	void Update(float delta) override;
	void InitializeGame() override;
	void WindowColision() override;
	void PlayerInput(float delta) override;
	void CheckGameWon() override;
	void SetGameOver(sf::Text& score, sf::Text& winner)override;

	void AIPaddleMovement(float delta);
	void CalculateScore(const Brick& brick, Player& player) override;
	void BallPaddleCollision(Ball& ball, Paddle& paddle, float testX, float testY) override;
	void BallBrickCollsision(Ball& ball, Brick& brick, float testX, float testY) override;
	sf::Vector2f ResetBallDirection(const Ball& ball) override;


private:
	bool IsBrickOverlapping(const Brick& brick, std::vector<Brick> bricks);
	uint8_t m_nrPlayers;
};