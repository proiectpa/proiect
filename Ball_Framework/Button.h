#pragma once
#include <SFML/Graphics.hpp>

class Button : public sf::Drawable
{
public:
	Button() = default;
	Button(std::string text, int characterSize, sf::Color textColor, sf::Texture& texture);
	
	void SetPosition(sf::Vector2f position);
	void SetFont(sf::Font& font);
	void SetTint(sf::Color color);
	void UpdateState(sf::Event& event, sf::RenderWindow& window);
	bool IsMouseOver(sf::RenderWindow& window);
	bool IsPressed();

	const sf::FloatRect& GetGlobalBounds() const;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	bool m_pressed;
	bool m_mouseOver;
	sf::Sprite m_button;
	sf::Text m_text;
};

