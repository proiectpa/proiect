#pragma once
#include<fstream>
#include "Game.h"

class Statistic
{
	class PlayerStats
	{
	public:
		PlayerStats(std::string name,int nrPlayedGames,int nrWonGames, uint8_t maximScore):
			m_name(name),
			m_nrPlayedGames(nrPlayedGames),
			m_nrWonGames(nrWonGames),
			m_maximScore(maximScore)
		{
		};
		std::string m_name;
		int m_nrPlayedGames;
		int m_nrWonGames;
		uint8_t m_maximScore;
	};
public:
	Statistic();
	void UpdateStatisitics(Game& game);
	void WriteStatistic(std::ofstream& gameType);
	void ReadStatistic(std::ifstream& gameType);
	const char* GameTypeToString(Game::GameType gameType);
private:
	std::vector<PlayerStats> m_playerStats;
	std::string m_Pong;
	std::string m_BrickBreaker;
};

