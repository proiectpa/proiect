#include "Textbox.h"

constexpr int textbox_width = 300;
constexpr int textbox_height = 65;
constexpr uint8_t const_limit = 15;

Textbox::Textbox(int size, sf::Color color, bool initialSelectedState)
{
	m_rectangle.setSize(sf::Vector2f(textbox_width, textbox_height));
	m_rectangle.setFillColor(sf::Color::Transparent);
	m_rectangle.setOutlineThickness(1);
	m_rectangle.setOutlineColor(sf::Color::White);
	textbox.setCharacterSize(size);
	textbox.setFillColor(color);
	m_attribute.setCharacterSize(size+10);
	m_attribute.setFillColor(color);
	isSelected = initialSelectedState;
	if (initialSelectedState)
	{
		textbox.setString("_");
	}
	else
	{
		textbox.setString("");
	}
	SetLimit(true, const_limit);
}

void Textbox::SetFont(sf::Font& font)
{
	textbox.setFont(font);
	m_attribute.setFont(font);
}

void Textbox::SetPosition(sf::Vector2f position)
{
	m_attribute.setPosition(sf::Vector2f(position.x - 170, position.y + 10));
	m_rectangle.setPosition(sf::Vector2f(position));
	textbox.setPosition(sf::Vector2f(position.x + 10, position.y + 10));
}

void Textbox::SetLimit(bool TrueOrFalse)
{
	hasLimit = TrueOrFalse;
}

void Textbox::SetLimit(bool TrueOrFalse, int limit)
{
	hasLimit = TrueOrFalse;
	this->limit = limit;
}

void Textbox::SetText(const std::string& text1)
{
	textbox.setString(text1);
	text.str(text1);
	text.clear();
}

void Textbox::SetAttribute(const std::string& attribute)
{
	m_attribute.setString(attribute);
}

void Textbox::SetColor(const sf::Color& color)
{
	m_rectangle.setOutlineColor(color);
}

void Textbox::Selected(bool selected)
{
	isSelected = selected;
	if (!selected)
	{
		std::string textInThisFunction = text.str();
		std::string newText = "";
		for (int index = 0; index < textInThisFunction.length(); index++)
		{
			newText += textInThisFunction[index];
		}
		textbox.setString(newText);
	}
	else
	{
		textbox.setString(text.str() + "_");
	}
}

std::string Textbox::GetText()
{
	return text.str();
}

const sf::Text& Textbox::GetAttribute() const
{
	return m_attribute;
}

const sf::FloatRect& Textbox::GetGlobalBounds() const
{
	return m_rectangle.getGlobalBounds();
}

void Textbox::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(this->m_attribute, states);
	target.draw(this->m_rectangle, states);
	target.draw(this->textbox, states);
}


void Textbox::TypedOn(sf::Event input)
{
	if (isSelected)
	{
		int characterTyped = input.text.unicode;
		if (characterTyped < 128)
		{
			if (hasLimit)
			{
				if (text.str().length() < limit)
				{
					InputLogic(characterTyped);
				}
				else if (text.str().length() >= limit && characterTyped == DELETE_KEY)
					DeleteLastCharacter();
			}
			else
			{
				InputLogic(characterTyped);
			}
		}
	}
}

bool Textbox::IsMouseOver(sf::RenderWindow& window)
{
	sf::FloatRect bounds = m_rectangle.getGlobalBounds();
	if (bounds.contains(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y))
		return true;
	return false;
}

void Textbox::InputLogic(int charTyped)
{
	if (charTyped != DELETE_KEY && charTyped != ENTER_KEY && charTyped != ESCAPE_KEY)
	{
		text << static_cast<char>(charTyped);
	}
	else if (charTyped == DELETE_KEY)
	{
		if (text.str().length() > 0)
		{
			DeleteLastCharacter();
		}
	}
	textbox.setString(text.str() + "_");
}

void Textbox::DeleteLastCharacter()
{
	std::string textInThisFunction = text.str();
	std::string newText = "";
	for (int index = 0; index < textInThisFunction.length() - 1; index++)
	{
		newText += textInThisFunction[index];
	}
	text.str("");
	text << newText;
	textbox.setString(text.str());
}
