#include "Game.h"
#include "GameRender.h"
#include <iostream>
#include <thread>
#include "Statistic.h"
#include "../Logger/Logging.h"

Game::Game(int difficulty)
{
	this->m_difficulty = difficulty;
	this->m_gameState = GameState::Playing;
	this->m_textures["Brick"].loadFromFile("../Resources/Brick.jpg");
	this->m_textures["Brick1"].loadFromFile("../Resources/Brick_1.jpg");
	this->m_textures["Brick2"].loadFromFile("../Resources/Brick_2.jpg");
	this->m_textures["Ball"].loadFromFile("../Resources/Ball.png");
	this->m_textures["Background"].loadFromFile("../Resources/Game_Background.jpg");
	this->m_textures["Paddle"].loadFromFile("../Resources/Paddle.png");
	this->m_textures["PaddleL"].loadFromFile("../Resources/PaddleLeft.png");
	this->m_textures["PaddleR"].loadFromFile("../Resources/PaddleRight.png");
	this->m_textures["Hearts"].loadFromFile("../Resources/Hearts.png");
	this->m_textures["PaddleB"].loadFromFile("../Resources/PaddleB.png");

	this->m_textures["BallDecreaseSpeed"].loadFromFile("../Resources/BallDecreaseSpeed.png");
	this->m_textures["BallIncreaseSpeed"].loadFromFile("../Resources/BallIncreaseSpeed.png");
	this->m_textures["PaddleIncreaseSize"].loadFromFile("../Resources/PaddleIncreaseSize.png");
	this->m_textures["PaddleDecreaseSize"].loadFromFile("../Resources/PaddleDecreaseSize.png");
	this->m_textures["PaddleIncreaseSpeed"].loadFromFile("../Resources/PaddleIncreaseSpeed.png");
	this->m_textures["PaddleDecreaseSpeed"].loadFromFile("../Resources/PaddleDecreaseSpeed.png");
	this->m_textures["BrickBecomesBall"].loadFromFile("../Resources/BrickBecomesBall.png");

	this->m_soundBuffers["BallBounce"].loadFromFile("../Resources/BallBounce.wav");
	this->m_soundBuffers["Victory"].loadFromFile("../Resources/Victory.wav");
	this->m_soundBuffers["Fail"].loadFromFile("../Resources/Fail.wav");

	this->m_sounds["BallBounce"].setBuffer(m_soundBuffers["BallBounce"]);
	this->m_sounds["Victory"].setBuffer(m_soundBuffers["Victory"]);
	this->m_sounds["Fail"].setBuffer(m_soundBuffers["Fail"]);

	m_sounds["BallBounce"].setVolume(40);
	m_sounds["Victory"].setVolume(40);
	m_sounds["Fail"].setVolume(40);

	this->m_background.setTexture(m_textures["Background"]);
	this->m_background.setScale(1 + (WindowWidth - m_background.getGlobalBounds().width) / m_background.getGlobalBounds().width, 1 + (WindowHeight - m_background.getGlobalBounds().height) / m_background.getGlobalBounds().height);
	this->m_scoreFont.loadFromFile("../Resources/arial.ttf");
}

Game::~Game()
{

}

void Game::Run(GameRender& render)
{
	sf::Clock clock;

	Logger::log("Game started", Logger::Level::Info);

	while (m_gameState != GameState::GameOver && m_gameState != GameState::GameInterrupted)
	{
		if (m_gameState == GameState::Playing)
		{
			sf::Event event;
			while (render.GetWindow().GetWindow().pollEvent(event))
			{
				render.GetWindow().ClosingWindow(event);
				if (!render.GetWindow().GetWindow().isOpen())
				{
					m_gameState = GameState::GameInterrupted;
				}
			}
			float delta = clock.restart().asSeconds();
			Update(delta);
			CheckGameWon();
			render.Render(*this);
		}
		if (m_gameState == GameState::Pause)
		{
			render.PauseMenu(*this);
			clock.restart();
		}
		if (m_gameState == GameState::GameOver)
		{
			render.GameOver(*this);
		}
	}
}

std::vector<Player>& Game::GetPlayers()
{
	return m_players;
}

std::vector<Ball>& Game::GetBalls()
{
	return m_balls;
}

void Game::SetGameState(GameState state)
{
	m_gameState = state;
}

std::unordered_map<std::string, Brick>& Game::GetBricks()
{
	return m_bricks;
}

std::unordered_map<std::string, sf::Sound>& Game::GetSounds()
{
	return m_sounds;
}

const Game::GameType& Game::GetGameType() const
{
	return m_gameType;
}

const Game::MultiplayerType& Game::GetMultiplayerType() const
{
	return m_multiplayerType;
}

void Game::BrickColision()
{
	for (auto& ball : m_balls)
	{
		for (auto brick = m_bricks.begin(); brick != m_bricks.end();)
		{
			sf::FloatRect brickBounds = brick->second.GetGlobalBounds();

			float ballCenterX = ball.GetX() + ball.GetRadius();
			float ballCenterY = ball.GetY() + ball.GetRadius();

			float testX = ballCenterX;
			float testY = ballCenterY;

			if (ballCenterX < brickBounds.left)
				testX = brickBounds.left;
			else if (ballCenterX > brickBounds.left + brickBounds.width)
				testX = brickBounds.left + brickBounds.width;

			if (ballCenterY < brickBounds.top)
				testY = brickBounds.top;
			else if (ballCenterY > brickBounds.top + brickBounds.height)
				testY = brickBounds.top + brickBounds.height;

			float deltaX = (ballCenterX - testX);
			float deltaY = (ballCenterY - testY);
			float distance = std::sqrt(deltaX * deltaX + deltaY * deltaY);

			int playerIndex = -1;

			if (distance <= ball.GetRadius())
			{
				m_sounds["BallBounce"].play();
				BallBrickCollsision(ball, brick->second, testX, testY);
			}

			if (brick->second.GetDurability() == 1)
			{
				brick->second.SetTexture(m_textures["Brick2"]);
			}
			if (brick->second.GetDurability() == 2)
			{
				brick->second.SetTexture(m_textures["Brick1"]);
			}
			if (brick->second.GetDurability() == 0)
			{
				if (playerIndex != -1)
				{
					m_players[playerIndex].AddDestroyedBrick();
				}
				if (playerIndex != -1)
				{
					CalculateScore(brick->second, m_players[playerIndex]);
				}
				brick = m_bricks.erase(brick);
			}
			else
			{
				++brick;
			}
		}
	}
}

void Game::PaddleColison()
{
	for (auto& ball : m_balls)
	{
		for (auto& player : m_players)
		{
			sf::FloatRect paddleBounds = player.GetPaddle().GetGlobalBounds();

			float ballCenterX = ball.GetX() + ball.GetRadius();
			float ballCenterY = ball.GetY() + ball.GetRadius();
			float testX = ballCenterX;
			float testY = ballCenterY;

			if (ballCenterX < paddleBounds.left)
				testX = paddleBounds.left;
			else if (ballCenterX > paddleBounds.left + paddleBounds.width)
				testX = paddleBounds.left + paddleBounds.width;

			if (ballCenterY < paddleBounds.top)
				testY = paddleBounds.top;
			else if (ballCenterY > paddleBounds.top + paddleBounds.height)
				testY = paddleBounds.top + paddleBounds.height;

			float deltaX = (ballCenterX - testX);
			float deltaY = (ballCenterY - testY);

			float distance = std::sqrt(deltaX * deltaX + deltaY * deltaY);
			if (distance <= ball.GetRadius())
			{
				m_sounds["BallBounce"].play();
				BallPaddleCollision(ball, player.GetPaddle(), testX, testY);
			}

		}
	}
}

void Game::BallMovementDelay(Ball& ball)
{
	sf::Vector2f temporary = ball.GetBallDirection();
	ball.SetBallDirection(sf::Vector2f(0, 0));
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	ball.SetBallDirection(temporary);
}
