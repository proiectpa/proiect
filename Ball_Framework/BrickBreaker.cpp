#include "BrickBreaker.h"
#include <thread>
#include <iostream>
#include "../Logger/Logging.h"

BrickBreaker::BrickBreaker(uint8_t difficulty, uint8_t nrPlayers, MultiplayerType multiplayerType, std::string player1, std::string player2) :
	Game(difficulty)
{
	m_multiplayerType = multiplayerType;
	m_balls.push_back(Ball(BallCoordX, BallCoordY, RadiusBall, CustomRed, m_textures["Ball"]));
	m_balls[0].SetBallDirection(sf::Vector2f(0, 2 * InitialBallSpeed));

	m_gameType = Game::GameType::BrickBreaker;

	m_players.push_back(Player(sf::Vector2f(InitialLeftPaddleX_BB, InitialPaddleY_BB), sf::Vector2f(PaddleLenght, PaddleHeight), m_scoreFont, player1, m_textures["Paddle"], m_textures["Hearts"]));
	m_players[0].SetSpeed(InitialPaddleSpeed);
	m_players[0].GetText()->setPosition(sf::Vector2f(WindowWidth / 5, WindowHeight - (WindowHeight / 3)));
	m_players[0].GetHearts().setPosition(sf::Vector2f(WindowWidth / 5, WindowHeight - (WindowHeight / 3) + m_players[0].GetText()->getGlobalBounds().height + 20));
	m_players[0].GetPaddle().SetColor(CustomRed);

	if (nrPlayers == 2)
	{
		m_players.push_back(Player(sf::Vector2f(InitialRightPaddleX_BB, InitialPaddleY_BB), sf::Vector2f(PaddleLenght, PaddleHeight), m_scoreFont, player2, m_textures["PaddleB"], m_textures["Hearts"]));
		m_players[1].SetSpeed(InitialPaddleSpeed);
		m_players[1].GetText()->setPosition(sf::Vector2f(WindowWidth - WindowWidth / 3, WindowHeight - (WindowHeight / 3)));
		m_players[1].GetHearts().setPosition(sf::Vector2f(WindowWidth - WindowWidth / 3, WindowHeight - (WindowHeight / 3) + m_players[0].GetText()->getGlobalBounds().height + 20));
		m_players[1].GetPaddle().SetColor(CustomBlue);
	}


	if (m_multiplayerType == MultiplayerType::Oponent)
	{
		m_balls[0].SetBallDirection(sf::Vector2f(-InitialBallSpeed, InitialBallSpeed));
		m_balls.push_back(Ball(BallCoordX + 5, BallCoordY, RadiusBall, CustomBlue, m_textures["Ball"]));
		m_balls[1].SetBallDirection(sf::Vector2f(InitialBallSpeed, InitialBallSpeed));
		m_targetNrBricks = m_bricks.size() / 2;
	}
	else
	{
		m_balls[0].SetBallDirection(sf::Vector2f(0, 2 * InitialBallSpeed));
		std::thread newThread(&Game::BallMovementDelay, this, std::ref(m_balls[0]));
		newThread.detach();
		m_balls[0].SetColor(CustomNeutralColor);
	}

	InitializeGame();
	m_targetNrBricks = m_bricks.size();
}

void BrickBreaker::Restart()
{
	m_gameState = GameState::Playing;
	m_bricks.clear();
	m_effects.clear();

	std::vector<Player> players;
	float coord = InitialLeftPaddleX_BB;
	sf::Vector2f text_pos(WindowWidth / 5, WindowHeight - (WindowHeight / 3));
	std::string texture = "Paddle";

	m_balls.clear();
	m_balls.push_back(Ball(BallCoordX-5, BallCoordY, RadiusBall, CustomRed, m_textures["Ball"]));
	m_balls[0].SetBallDirection(sf::Vector2f(0, 2*InitialBallSpeed));

	if (m_multiplayerType == MultiplayerType::Oponent)
	{
		m_balls[0].SetBallDirection(sf::Vector2f(-InitialBallSpeed, InitialBallSpeed));
		m_balls[0].SetColor(m_players[0].GetPaddle().GetColor());
		m_balls.push_back(Ball(BallCoordX + 5, BallCoordY, RadiusBall, CustomBlue, m_textures["Ball"]));
		m_balls[1].SetBallDirection(sf::Vector2f(InitialBallSpeed, InitialBallSpeed));
		m_balls[1].SetColor(m_players[1].GetPaddle().GetColor());
		std::thread newThread(&Game::BallMovementDelay, this, std::ref(m_balls[1]));
		newThread.detach();
	}
	else
	{
		m_balls[0].SetColor(CustomNeutralColor);
	}

	std::thread newThread(&Game::BallMovementDelay, this, std::ref(m_balls[0]));
	newThread.detach();

	int index = 0;
	for (auto& player : m_players)
	{
		players.push_back(Player(sf::Vector2f(coord, InitialPaddleY_BB), sf::Vector2f(PaddleLenght, PaddleHeight), m_scoreFont, player.GetName(), m_textures[texture], m_textures["Hearts"]));
		coord = InitialRightPaddleX_BB;
		players[index].GetText()->setPosition(text_pos);
		players[index].GetHearts().setPosition(text_pos.x, text_pos.y + m_players[0].GetText()->getGlobalBounds().height + 20);
		players[index].GetPaddle().SetColor(m_players[index].GetPaddle().GetColor());
		text_pos.x = WindowWidth - WindowWidth / 3;
		texture = "PaddleB";

		players[index++].SetSpeed(InitialPaddleSpeed);
	}

	m_players.clear();
	m_players = players;

	InitializeGame();
}

void BrickBreaker::BallWindowCollision(Ball& ball)
{
	if (m_balls.size() > 1 && m_multiplayerType == MultiplayerType::Collaborator)
	{
		for (int i = 0; i < m_balls.size(); i++)
		{
			if (&m_balls[i] == &ball)
			{
				m_balls.erase(m_balls.begin() + i);
				break;
			}
		}
	}
	if (m_balls.size() > 2 && m_multiplayerType == MultiplayerType::Oponent)
	{
		std::vector<Ball>::iterator ball_Iterator;
		uint8_t redBalls = 0;
		for (auto it = m_balls.begin(); it != m_balls.end(); ++it)
		{
			if (it->GetColor() == CustomRed)
			{
				++redBalls;
			}
			if (&*it == &ball)
			{
				ball_Iterator = it;
			}
		}
		if (ball.GetColor() == CustomRed && redBalls > 1)
		{
			m_balls.erase(ball_Iterator);
		}
		else if (ball.GetColor() == CustomBlue && (m_balls.size() - redBalls) > 1)
		{
			m_balls.erase(ball_Iterator);
		}
	}
	if (m_balls.size() == 1 || (m_multiplayerType == MultiplayerType::Oponent && m_balls.size() == 2))
	{
		if (ball.GetColor() == CustomNeutralColor)
		{
			for (auto& player : m_players)
			{
				player.DecreaseLives();
			}
		}
		else
		{
			if (ball.GetColor() == m_players[0].GetPaddle().GetColor())
			{
				m_players[0].DecreaseLives();
			}
			else if (m_players.size() == 2 && ball.GetColor() == m_players[1].GetPaddle().GetColor())
			{
				m_players[1].DecreaseLives();
			}
		}
		ball.SetPosition(WindowWidth / 2, WindowHeight / 2);
		if (ball.GetColor() == CustomRed)
		{
			ball.SetPosition(WindowWidth / 2 - 10, WindowHeight / 2);
		}
		else
		{
			ball.SetPosition(WindowWidth / 2 + 10, WindowHeight / 2);
		}
		ball.SetBallDirection(sf::Vector2f(0, 2 * InitialBallSpeed));
	}
}

void BrickBreaker::ResetEffect(Ball& ball, Player& player, Brick::Effect effect)
{
	sf::sleep(sf::seconds(5));
	switch (effect)
	{
	case Brick::Effect::PaddleIncreaseSize:
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(PaddleLenght, PaddleHeight));
		}
		break;
	case Brick::Effect::PaddleDecreaseSize:
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(PaddleLenght, PaddleHeight));
		}
		break;
	case Brick::Effect::PaddleIncreaseSpeed:
		if (&player != nullptr)
		{
			player.SetSpeed(InitialPaddleSpeed);
		}
		break;
	case Brick::Effect::BallDecreaseSpeed:
		if (&ball != nullptr)
		{
			ball.SetBallDirection(ResetBallDirection(ball));
		}
		break;
	case Brick::Effect::BallIncreaseSpeed:
		if (&ball != nullptr)
		{
			ball.SetBallDirection(ResetBallDirection(ball));
		}
		break;
	case Brick::Effect::PaddleDecreaseSpeed:
		if (&player != nullptr)
		{
			player.SetSpeed(InitialPaddleSpeed);
		}
		break;
	default:
		break;
	}
	m_effects.erase(effect);
}

void BrickBreaker::ApplyEffect(Brick::Effect effect, Ball& ball, Player& player)
{
	switch (effect)
	{
	case Brick::Effect(-1):
	{
		ball.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x * 1.3, ball.GetBallDirection().y * 1.3));
		break;
	}
	case Brick::Effect(3):
	{
		ball.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x * 0.7, ball.GetBallDirection().y * 0.7));
		break;
	}
	case Brick::Effect(1):
	{
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(player.GetPaddle().GetLenght() * 1.2, player.GetPaddle().GetHeight()));
		}
		break;
	}
	case Brick::Effect(-2):
	{
		if (&player != nullptr)
		{
			player.SetPaddleDimension(sf::Vector2f(player.GetPaddle().GetLenght() * 0.8, player.GetPaddle().GetHeight()));
		}
		break;
	}
	case Brick::Effect(2):
	{
		if (&player != nullptr)
		{
			player.IncreaseSpeed();
		}
		break;
	}
	case Brick::Effect(-3):
	{
		if (&player != nullptr)
		{
			player.DecreaseSpeed();
		}
		break;
	}
	case Brick::Effect(-4):
	{
		Ball newBall(ball.GetX(), ball.GetY(), RadiusBall, ball.GetColor(), m_textures["Ball"]);
		newBall.SetBallDirection(sf::Vector2f(ball.GetBallDirection().x, -ball.GetBallDirection().y));
		if (m_multiplayerType == MultiplayerType::Oponent)
		{
			newBall.SetColor(player.GetPaddle().GetColor());
		}
		else
		{
			newBall.SetColor(ball.GetColor());
		}
		m_balls.push_back(newBall);
		break;
	}
	default:
		break;
	}
}

void BrickBreaker::Update(float delta)
{
	PlayerInput(delta);

	WindowColision();
	BrickColision();
	PaddleColison();

	if (m_bricks.size() == 0)
	{
		m_gameState = GameState::GameOver;
	}

	if (m_multiplayerType == MultiplayerType::Oponent)
	{
		if (m_players[0].GetDestroyedBricks() == m_targetNrBricks)
		{
			m_gameState = GameState::GameOver;
		}
		else if (m_players[1].GetDestroyedBricks() == m_targetNrBricks)
		{
			m_gameState = GameState::GameOver;
		}
	}

	for (Ball& m_ball : m_balls)
	{
		m_ball.Move(delta);
	}
}

void BrickBreaker::InitializeGame()
{
	sf::Color brick_color = CustomRed;
	const float spaceX = 10.0f;
	const float spaceY = 8.0f;
	uint8_t bricksPerRow = 5 + m_difficulty;
	uint8_t bricksPerColumn = 2 + m_difficulty;

	uint8_t nrAdvantages = m_difficulty * 5;
	uint8_t nrDisadvantages = m_difficulty * 4 + m_difficulty * 2;
	uint8_t brickSpace = (WindowHeight / (4 - (m_difficulty / 2)));
	uint8_t nrToughBricks = 5 * m_difficulty;

	std::uniform_int_distribution<int> distribAdvantages(1, 3);
	std::uniform_int_distribution<int> distribDisadvantages(-4, -1);
	std::uniform_int_distribution<int> distribDurability(2, 3);

	std::random_device rand;
	std::vector<Brick> bricks;
	std::vector<std::pair<int, int>> coordinates;

	float brick_height = brickSpace / bricksPerColumn - spaceY * 2;
	float brick_width = WindowWidth / bricksPerRow - spaceX * 2;

	int coordX = 0;
	int coordY = 0;

	uint8_t currentNrAdvantages = 0;
	uint8_t currentNrDisadvantages = 0;
	uint8_t curentNrToughBricks = 0;

	for (int row = 0; row < bricksPerColumn; ++row)
	{
		coordY = row * brickSpace / bricksPerColumn;

		for (int column = 0; column < bricksPerRow; ++column)
		{
			coordX = column * WindowWidth / bricksPerRow;

			Brick::Effect effect = Brick::Effect(0);
			uint8_t durability = 1;

			if (curentNrToughBricks++ < nrToughBricks)
			{
				durability = distribDurability(rand);
			}

			if (currentNrAdvantages++ < nrAdvantages)
			{
				effect = Brick::Effect(distribAdvantages(rand));
			}
			else if (currentNrDisadvantages++ < nrDisadvantages)
			{
				effect = Brick::Effect(distribDisadvantages(rand));
			}

			if (this->m_multiplayerType == MultiplayerType::Oponent && (bricks.size() >= (bricksPerColumn * bricksPerRow) / 2))
			{
				brick_color = CustomBlue;
			}

			coordinates.push_back({ coordX + spaceX, coordY + spaceY });

			Brick brick(sf::Vector2f(0, 0), sf::Vector2f(brick_width, brick_height), durability, brick_color, effect, m_textures["Brick"]);

			bricks.push_back(brick);
		}
	}

	std::random_shuffle(coordinates.begin(), coordinates.end());
	int index = 0;

	for (Brick& brick : bricks)
	{
		if (brick.EffectToString() != "")
		{
			brick.SetEffectTexture(m_textures[brick.EffectToString()]);
		}
		brick.SetPosition(coordinates[index].first, coordinates[index].second);
		brick.UpdateEffectPosition();
		brick.GenerateID();
		this->m_bricks[brick.GetID()] = brick;
		++index;
	}

	if (m_bricks.empty() || m_balls.empty() ||m_players.empty())
	{
		Logger::log("Intialization for BrickBreaker failed", Logger::Level::Error);
	}
	else
	{
		Logger::log("Intialization for BrickBreaker Complete", Logger::Level::Info);
	}
}

void BrickBreaker::WindowColision()
{
	for (Ball& m_ball : m_balls)
	{
		if (m_ball.GetY() + m_ball.GetRadius() >= WindowHeight)
		{
			m_ball.SetPosition(m_ball.GetX(), WindowHeight - m_ball.GetRadius() - 1.0f);
			m_ball.SetBallDirection(sf::Vector2f(m_ball.GetBallDirection().x, -m_ball.GetBallDirection().y));
			BallWindowCollision(m_ball);
		}
		if (m_ball.GetX() + m_ball.GetRadius() >= WindowWidth)
		{
			m_sounds["BallBounce"].play();
			m_ball.SetPosition(WindowWidth - m_ball.GetRadius() - 1.0f, m_ball.GetY());
			m_ball.SetBallDirection(sf::Vector2f(-m_ball.GetBallDirection().x, m_ball.GetBallDirection().y));
		}
		if (m_ball.GetY() <= 0)
		{
			m_sounds["BallBounce"].play();
			m_ball.SetPosition(m_ball.GetX(), 1.0f);
			m_ball.SetBallDirection(sf::Vector2f(m_ball.GetBallDirection().x, -m_ball.GetBallDirection().y));
		}
		if (m_ball.GetX() <= 0)
		{
			m_sounds["BallBounce"].play();
			m_ball.SetPosition(1.0f, m_ball.GetY());
			m_ball.SetBallDirection(sf::Vector2f(-m_ball.GetBallDirection().x, m_ball.GetBallDirection().y));
		}
	}
}

void BrickBreaker::PlayerInput(float delta)
{
	if (m_players[0].GetLives() > 0)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			if (m_players[0].GetPaddle().GetX() + delta * m_players[0].GetSpeed() < WindowWidth - m_players[0].GetPaddle().GetLenght())
			{
				m_players[0].Move(sf::Vector2f(delta * m_players[0].GetSpeed(), 0));
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			if (m_players[0].GetPaddle().GetX() - (delta * m_players[0].GetSpeed()) > 0)
			{
				m_players[0].Move(sf::Vector2f(-(delta * m_players[0].GetSpeed()), 0));
			}
		}
	}
	if (this->m_players.size() == 2 && m_players[1].GetLives() > 0)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && m_players.size() == 2)
		{
			if (m_players[1].GetPaddle().GetX() + delta * m_players[1].GetSpeed() < WindowWidth - m_players[1].GetPaddle().GetLenght())
			{
				m_players[1].Move(sf::Vector2f(delta * m_players[1].GetSpeed(), 0));
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && m_players.size() == 2)
		{
			if (m_players[1].GetPaddle().GetX() - (delta * m_players[1].GetSpeed()) > 0)
			{
				m_players[1].Move(sf::Vector2f(-(delta * m_players[1].GetSpeed()), 0));
			}
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		m_gameState = GameState::Pause;
	}
}

void BrickBreaker::CalculateScore(const Brick& brick, Player& player)
{
	if (brick.GetDurability() != 0)
	{
		player.AddPointsToScore(brick.GetDurability() * 10);
	}
	else
	{
		player.AddPointsToScore(m_difficulty * 15);
	}
}

void BrickBreaker::BallPaddleCollision(Ball& ball, Paddle& paddle, float testX, float testY)
{

	float ballCenterX = ball.GetX() + ball.GetRadius();
	float ballCenterY = ball.GetY() + ball.GetRadius();

	if (m_multiplayerType != Game::MultiplayerType::Oponent)
	{
		ball.SetColor(paddle.GetColor());
	}

	sf::FloatRect paddleBounds = paddle.GetGlobalBounds();
	float delta, ratio;
	delta = (ballCenterX)-(paddleBounds.left + paddleBounds.width / 2);
	ratio = std::abs(delta) / (paddleBounds.width / 2);

	float speed = std::abs(ball.GetBallDirection().x) + std::abs(ball.GetBallDirection().y);

	if (ratio > 0.65f) ratio = 0.65f;
	if (ratio < 0.2f) ratio = 0.2f;

	float x = ratio * speed;
	float y = (1 - ratio) * speed;


	if (ball.GetBallDirection().x <= 0) x = -x;
	sf::Vector2f direction(x, -y);
	ball.SetBallDirection(direction);


	if (testY == paddleBounds.top)
	{
		ball.SetPosition(ball.GetX(), paddleBounds.top - ball.GetRadius() * 2 - 1);
	}
	if (testX == paddleBounds.left || testX == paddleBounds.left + paddleBounds.width)
	{
		if (testX == paddleBounds.left)
		{
			ball.SetPosition(paddleBounds.left - ball.GetRadius() * 2 - 1, ball.GetY());
		}
		else if (testX == paddleBounds.left + paddleBounds.width)
		{
			ball.SetPosition(paddleBounds.left + paddleBounds.width + 1, ball.GetY());
		}
	}
}

void BrickBreaker::BallBrickCollsision(Ball& ball, Brick& brick, float testX, float testY)
{
	sf::FloatRect brickBounds = brick.GetGlobalBounds();
	int playerIndex = -1;
	for (int index = 0; index < m_players.size(); ++index)
	{
		if (ball.GetColor() == m_players[index].GetPaddle().GetColor())
		{
			playerIndex = index;
		}
	}

	if (ball.GetColor() == brick.GetColor() || m_multiplayerType == MultiplayerType::Collaborator)
	{
		if (playerIndex != -1)
		{
			CalculateScore(brick, m_players[playerIndex]);
		}

		brick.DecreaseDurability();
		if (brick.GetEffect() == Brick::Effect(-4))
		{
			if (brick.GetDurability() == 0)
			{
				ApplyEffect(brick.GetEffect(), ball, m_players[playerIndex]);
				brick.SetEffect(Brick::Effect(0));
			}
		}
		else
		{
			if (m_effects.find(brick.GetEffect()) == m_effects.end())
			{
				m_effects[brick.GetEffect()] = true;
				ApplyEffect(brick.GetEffect(), ball, m_players[playerIndex]);
				std::thread newThread(&Game::ResetEffect, this, std::ref(ball), std::ref(m_players[playerIndex]), brick.GetEffect());
				newThread.detach();
				brick.SetEffect(Brick::Effect(0));
			}
		}
	}

	if (testY == brickBounds.top || testY == brickBounds.top + brickBounds.height)
	{
		sf::Vector2f direction(ball.GetBallDirection().x, -ball.GetBallDirection().y);
		ball.SetBallDirection(direction);
		if (testY == brickBounds.top)
		{
			ball.SetPosition(ball.GetX(), brickBounds.top - ball.GetRadius() * 2 - 1);
		}
		else if (testY == brickBounds.top + brickBounds.height)
		{
			ball.SetPosition(ball.GetX(), brickBounds.top + brickBounds.height + 1);
		}
	}
	else if (testX == brickBounds.left || testX == brickBounds.left + brickBounds.width)
	{
		sf::Vector2f direction(-ball.GetBallDirection().x, ball.GetBallDirection().y);
		ball.SetBallDirection(direction);
		if (testX == brickBounds.left)
		{
			ball.SetPosition(brickBounds.left - ball.GetRadius() * 2 - 1, ball.GetY());
		}
		else if (testX == brickBounds.left + brickBounds.width)
		{
			ball.SetPosition(brickBounds.left + brickBounds.width + 1, ball.GetY());
		}
	}
}

void BrickBreaker::CheckGameWon()
{
	uint8_t condition = 0;
	for (int index = 0; index < m_players.size(); ++index)
	{
		if (m_players[index].GetLives() == 0 || !m_players[index].IsWinner())
		{
			++condition;
			m_players[index].SetAsLoser();
			if (m_players.size() == 2 && m_multiplayerType == Game::MultiplayerType::Collaborator)
			{
				m_balls[0].SetColor(m_players[1 - index].GetPaddle().GetColor());
			}
		}
	}
	if (condition == m_players.size() && m_multiplayerType == Game::MultiplayerType::Collaborator)
	{
		m_gameState = Game::GameState::GameOver;
	}
	else if (condition != 0 && m_multiplayerType == Game::MultiplayerType::Oponent)
	{
		m_gameState = Game::GameState::GameOver;
	}
}

void BrickBreaker::SetGameOver(sf::Text& score, sf::Text& winner)
{
	int condition = 0;
	if (m_bricks.size() == 0)
	{
		m_sounds["Victory"].play();
	}
	else
	{
		m_sounds["Fail"].play();

	}
	for (auto player : m_players)
	{
		if (player.IsWinner())
		{
			++condition;
		}
	}
	if (condition == 0)
	{
		if (m_players.size() == 1)
		{
			winner.setString("GAME OVER");
			score.setString(" The score is: " + std::to_string(m_players[0].GetScore()));
		}
		else
		{
			winner.setString("GAME OVER");
		}
	}
	else if (m_multiplayerType == Game::MultiplayerType::Collaborator && m_players.size() == 2)
	{
		winner.setString("GAME WON");
		score.setString(" The score is: " + std::to_string(m_players[0].GetScore() + m_players[1].GetScore()));
	}
	else if (condition == 1)
	{
		for (auto player : m_players)
		{
			if (player.IsWinner())
			{
				winner.setString("The winner is: " + player.GetName());
				score.setString(" The score is: " + std::to_string(player.GetScore()));
				break;
			}
		}
	}
}

sf::Vector2f BrickBreaker::ResetBallDirection(const Ball& ball)
{
	float ratio = std::abs(ball.GetBallDirection().x) / (std::abs(ball.GetBallDirection().x) + std::abs(ball.GetBallDirection().y));
	return sf::Vector2f(ratio * InitialBallSpeed * 2 * (ball.GetBallDirection().x / std::abs(ball.GetBallDirection().x)),
		(1 - ratio) * InitialBallSpeed * 2 * (ball.GetBallDirection().y / std::abs(ball.GetBallDirection().y)));

}
